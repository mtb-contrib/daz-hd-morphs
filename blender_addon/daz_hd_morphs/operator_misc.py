import bpy, os
from . import utils

def initialize_tiles_list(context):
    tl = context.scene.daz_hd_morph.texture_tiles
    if len(tl.entries) > 0:
        return
    tl.entries.clear()
    for tn in range(1001, 1011):
        entry = tl.entries.add()
        entry.name = str(tn)
        entry.enabled = True
    tl.index = 0


class TilesListDisableAll(bpy.types.Operator):
    bl_idname = "dazhdmorph.tiles_op_disable_all"
    bl_label = "Disable all tiles"
    bl_description = "Disable all tiles"

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        tl = context.scene.daz_hd_morph.texture_tiles
        for entry in tl.entries:
            entry.enabled = False
        return {"FINISHED"}

class TilesListEnableAll(bpy.types.Operator):
    bl_idname = "dazhdmorph.tiles_op_enable_all"
    bl_label = "Enable all tiles"
    bl_description = "Enable all tiles"

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        tl = context.scene.daz_hd_morph.texture_tiles
        for entry in tl.entries:
            entry.enabled = True
        return {"FINISHED"}


class MorphFilesDuplicate(bpy.types.Operator):
    bl_idname = "dazhdmorph.morph_files_op_dup"
    bl_label = "Duplicate active item"
    bl_description = "Duplicate active list item"

    @classmethod
    def poll(cls, context):
        return context.scene.daz_hd_morph.morph_files_list.index >= 0

    def execute(self, context):
        mfl = context.scene.daz_hd_morph.morph_files_list
        active_index = mfl.index
        active_item = mfl.entries[active_index]

        entry = mfl.entries.add()
        entry.filepath = active_item.filepath
        entry.group = active_item.group
        entry.weight = active_item.weight
        entry.morph_type = active_item.morph_type

        new_index = active_index + 1
        mfl.entries.move(len(mfl.entries)-1, new_index)
        mfl.index = new_index

        return {"FINISHED"}


class MorphFilesAdd(bpy.types.Operator):
    bl_idname = "dazhdmorph.morph_files_op_add"
    bl_label = "Add file"
    bl_description = "Add file to list"

    files : bpy.props.CollectionProperty( name="Filepaths",
                        type=bpy.types.OperatorFileListElement )

    directory : bpy.props.StringProperty( subtype='DIR_PATH' )

    filter_glob : bpy.props.StringProperty( default="*.dsf",
                                            options={'HIDDEN'} )

    @classmethod
    def poll(cls, context):
        return True

    def invoke(self, context, event):
        default_dir = context.scene.daz_hd_morph.default_morph_dir
        if default_dir and os.path.isdir(default_dir):
            self.directory = default_dir
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}

    @staticmethod
    def get_empty_groups_gen(entries):
        groups = set([e.group for e in entries])
        def generator():
            g = 0
            while True:
                if g not in groups:
                    yield g
                g += 1
        return generator()

    def execute(self, context):
        if len(self.files) == 0:
            return {'CANCELLED'}
        mfl = context.scene.daz_hd_morph.morph_files_list
        empty_g = self.get_empty_groups_gen(mfl.entries)
        for f in self.files:
            entry = mfl.entries.add()
            entry.filepath = os.path.join(self.directory, f.name)
            entry.group = next(empty_g)
        mfl.index = len(mfl.entries)-1
        return {"FINISHED"}

class MorphFilesRemove(bpy.types.Operator):
    bl_idname = "dazhdmorph.morph_files_op_remove"
    bl_label = "Remove file"
    bl_description = "Remove selected file from list"

    @classmethod
    def poll(cls, context):
        return context.scene.daz_hd_morph.morph_files_list.index >= 0

    def execute(self, context):
        mfl = context.scene.daz_hd_morph.morph_files_list
        mfl.entries.remove(mfl.index)
        mfl.index -= 1
        return {"FINISHED"}


class MorphFilesClear(bpy.types.Operator):
    bl_idname = "dazhdmorph.morph_files_op_clear"
    bl_label = "Remove all files"
    bl_description = "Remove all files from list"

    @classmethod
    def poll(cls, context):
        return len(context.scene.daz_hd_morph.morph_files_list.entries) > 0

    def execute(self, context):
        mfl = context.scene.daz_hd_morph.morph_files_list
        mfl.entries.clear()
        mfl.index = -1
        return {"FINISHED"}

def get_mfl_groups_dict(entries):
    groups = {}
    for e in entries:
        if e.group not in groups:
            groups[e.group] = [0, 0]
        groups[e.group][0] += e.weight
        groups[e.group][1] += 1
    return groups

class MorphFilesNormalize(bpy.types.Operator):
    bl_idname = "dazhdmorph.morph_files_op_normalize"
    bl_label = "Normalize weights"
    bl_description = "Normalize all weights"

    @classmethod
    def poll(cls, context):
        return len(context.scene.daz_hd_morph.morph_files_list.entries) > 0

    def execute(self, context):
        mfl = context.scene.daz_hd_morph.morph_files_list
        groups = get_mfl_groups_dict(mfl.entries)
        for e in mfl.entries:
            g_sum, g_len = groups[e.group]
            if g_sum < 1e-4:
                e.weight = 1.0/g_len
            else:
                e.weight /= g_sum
        return {"FINISHED"}

class MorphFilesNormalizeUniform(bpy.types.Operator):
    bl_idname = "dazhdmorph.morph_files_op_normalize_unif"
    bl_label = "Uniform weights"
    bl_description = "Uniform normalized weights"

    @classmethod
    def poll(cls, context):
        return len(context.scene.daz_hd_morph.morph_files_list.entries) > 0

    def execute(self, context):
        mfl = context.scene.daz_hd_morph.morph_files_list
        groups = get_mfl_groups_dict(mfl.entries)
        for e in mfl.entries:
            e.weight = 1.0/groups[e.group][1]
        return {"FINISHED"}

class MorphFilesExclusive(bpy.types.Operator):
    bl_idname = "dazhdmorph.morph_files_op_exclusive"
    bl_label = "Enable only selected"
    bl_description = "Set weight of selected to 1 and all others to 0"

    @classmethod
    def poll(cls, context):
        return context.scene.daz_hd_morph.morph_files_list.index >= 0

    def execute(self, context):
        mfl = context.scene.daz_hd_morph.morph_files_list
        for entry in mfl.entries:
            entry.weight = 0
        mfl.entries[mfl.index].weight = 1
        return {"FINISHED"}
