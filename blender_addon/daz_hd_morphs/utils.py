import bpy, os, math, time, gzip, json


def remove_obj_and_mtl(filepath):
    if not has_extension(filepath, "obj"):
        return
    if os.path.isfile(filepath):
        os.remove(filepath)
    mtl_filepath = filepath[:-4] + ".mtl"
    if os.path.isfile(mtl_filepath):
        os.remove(mtl_filepath)

def is_in_subdir_of(filepath, dirpath):
    dirpath2 = os.path.abspath( bpy.path.abspath(os.path.dirname(filepath)) )
    dirpath = os.path.abspath( bpy.path.abspath(dirpath) )
    return os.path.commonpath([dirpath]) == os.path.commonpath([dirpath, dirpath2])

def has_extension(filepath, *exts):
    assert(len(exts) > 0)
    t = os.path.basename(filepath).rsplit(".", 1)
    return (len(t) == 2 and t[1].lower() in exts)

def get_new_filename(dirpath, prefix, ext):
    i = 0
    if ext is None:
        ext = ""
    else:
        ext = ".{0}".format(ext)
    while True:
        filepath = os.path.join( dirpath, "{0}_{1}{2}".format(prefix, i, ext) )
        if not os.path.isfile(filepath):
            return filepath
        i += 1

def makeValidFilename(name):
    name = name.strip().lower()
    return "".join( c for c in name if (c.isalnum() or c in "._- ") )

def getMorphName(morph_filepath):
    n = os.path.basename(morph_filepath)
    t = n.rsplit(".", 1)
    return makeValidFilename( "_".join(t) )

def getMorphsGroupsFilenames(morphs_groups, suffix=""):
    morphs_groups_names = {}
    for g, morph_list in morphs_groups.items():
        assert(len(morph_list) > 0)
        if len(morph_list) == 1:
            morphs_groups_names[g] = "{0}{1}".format(getMorphName(morph_list[0][0]), suffix)
        else:
            morphs_groups_names[g] = "morphs_mixture_g{0}{1}".format(g, suffix)
    return morphs_groups_names

def getMorphsGroupsShapeKeyNames(morphs_groups):
    morphs_groups_names = {}
    for g, morph_list in morphs_groups.items():
        assert(len(morph_list) > 0)
        if len(morph_list) == 1:
            morphs_groups_names[g] = "{0}{1}".format(getMorphName(morph_list[0][0]), suffix)
        else:
            morphs_groups_names[g] = "morphs_mixture_g{0}{1}".format(g, suffix)
    return morphs_groups_names

def startProgress(string):
    print(string)
    wm = bpy.context.window_manager
    wm.progress_begin(0, 100)

def endProgress():
    wm = bpy.context.window_manager
    wm.progress_update(100)
    wm.progress_end()

def showProgress(n, total):
    pct = (100.0*n)/total
    wm = bpy.context.window_manager
    wm.progress_update(int(pct))

def delete_object(ob):
    assert(ob.type == 'MESH')
    ob_data = ob.data
    ob_mats = set(ob_data.materials)
    bpy.data.objects.remove(ob, do_unlink=True)
    if ob_data is not None and ob_data.users == 0:
        bpy.data.meshes.remove(ob_data, do_unlink=True)
    for m in ob_mats:
        if m is not None and m.users == 0:
            bpy.data.materials.remove(m, do_unlink=True)

def delete_armature(ob):
    assert(ob.type == 'ARMATURE')
    ob_data = ob.data
    bpy.data.objects.remove(ob, do_unlink=True)
    if ob_data is not None and ob_data.users == 0:
        bpy.data.armatures.remove(ob_data, do_unlink=True)

def delete_object_materials(ob):
    mats = set(ob.data.materials)
    ob.data.materials.clear()
    for m in mats:
        if m is not None and m.users == 0:
            bpy.data.materials.remove(m, do_unlink=True)

def make_single_active(ob):
    bpy.ops.object.select_all(action='DESELECT')
    ob.select_set(True)
    bpy.context.view_layer.objects.active = ob

def make_final_active(ob):
    arm = get_armature(ob)
    if arm:
        make_single_active(arm)
    else:
        make_single_active(ob)

def export_obj(ob, filepath, write_materials=False, use_modifiers=False):
    assert(os.path.isdir(os.path.dirname(filepath)))
    assert(len(filepath) > 4 and filepath[-4:] == ".obj")
    mtl_filepath = filepath[:-4] + ".mtl"

    make_single_active(ob)
    bpy.ops.export_scene.obj( filepath=filepath,
                              check_existing=False, use_selection=True, use_animation=False,
                              use_mesh_modifiers=use_modifiers, use_normals=False, use_smooth_groups=False,
                              use_uvs=True, use_materials=write_materials, use_triangles=False, use_vertex_groups=False,
                              keep_vertex_order=True, global_scale=100.0, path_mode='ABSOLUTE',
                              axis_forward='-Z', axis_up='Y' )
    if os.path.isfile(mtl_filepath):
        os.remove(mtl_filepath)
    if os.path.isfile(filepath):
        return filepath
    return None

def import_dll_obj(filepath, gScale):
    assert(has_extension(filepath, "obj"))
    if not os.path.isfile(filepath):
        raise ValueError(".obj \"{0}\" not found.".format(filepath))
    bpy.ops.object.select_all(action='DESELECT')
    bpy.ops.import_scene.obj( filepath=filepath, filter_glob='*.obj', use_image_search=False,
                              split_mode='OFF', use_split_objects=False, use_split_groups=False,
                              use_groups_as_vgroups=False, axis_forward='-Z', axis_up='Y' )
    hd_ob = bpy.context.selected_objects[0]
    hd_ob.rotation_euler[0] = 0
    hd_ob.scale *= gScale
    hd_ob.select_set(True)
    bpy.ops.object.transform_apply(location=False, rotation=False, scale=True)
    bpy.ops.object.shade_smooth()
    bpy.ops.object.select_all(action='DESELECT')
    return hd_ob

def import_dll_collada(filepath):
    assert(has_extension(filepath, "dae"))
    if not os.path.isfile(filepath):
        raise ValueError(".dae \"{0}\" not found.".format(filepath))
    bpy.ops.object.select_all(action='DESELECT')
    bpy.ops.wm.collada_import( filepath=filepath, import_units=False, fix_orientation=False,
                               find_chains=False, auto_connect=False, min_chain_length=0,
                               keep_bind_info=False )
    hd_ob = None
    for ob in bpy.context.selected_objects:
        if ob.type == 'ARMATURE':
            delete_armature(ob)
        else:
            remove_armature_modifiers(ob)
            hd_ob = ob
    bpy.ops.object.shade_smooth()
    bpy.ops.object.select_all(action='DESELECT')
    return hd_ob

def add_collection(coll_name):
    if coll_name not in bpy.data.collections:
        bpy.data.collections.new(coll_name)
    coll = bpy.data.collections[coll_name]
    if coll.name not in bpy.context.scene.collection.children:
        bpy.context.scene.collection.children.link(coll)
    return coll

def move_to_collection(ob, coll_name):
    if coll_name is not None:
        coll_dest = add_collection(coll_name)
    else:
        coll_dest = bpy.context.window.view_layer.layer_collection.collection
    for coll in ob.users_collection:
        coll.objects.unlink(ob)
    coll_dest.objects.link(ob)

def only_enable_collection(coll_name):
    view_layer = bpy.context.window.view_layer
    for layer_coll in view_layer.layer_collection.children:
        layer_coll.exclude = (layer_coll.name != coll_name)
                #(layer_coll.collection not in ob.users_collection)

def copy_object(ob, collection_name=None):
    ob2 = ob.copy()
    ob2.data = ob.data.copy()
    move_to_collection(ob2, collection_name)
    return ob2

def get_armature(ob):
    for m in ob.modifiers:
        if m.type == 'ARMATURE':
            return m.object
    return None

def apply_data_transfer_modifier(ob_base, ob_hd):
    dt = ob_hd.modifiers.new("dazHDmorph_dataTransfer", 'DATA_TRANSFER')
    dt.object = ob_base
    dt.mix_mode = 'REPLACE'
    dt.mix_factor = 1
    dt.use_vert_data = True
    dt.data_types_verts = {'VGROUP_WEIGHTS'}
    dt.vert_mapping = 'NEAREST'
    dt.layers_vgroup_select_src = 'ALL'
    dt.layers_vgroup_select_dst = 'NAME'
    dt.use_edge_data = False
    dt.use_loop_data = False
    dt.use_poly_data = False
    dt.use_max_distance = False
    # ~ dt.use_max_distance = True
    # ~ dt.max_distance = 0.1
    dt.ray_radius = 0

    make_single_active(ob_hd)
    bpy.ops.object.datalayout_transfer(modifier=dt.name)
    bpy.ops.object.modifier_apply(modifier=dt.name)


def apply_data_transfer_operator(ob_base, ob_hd):
    make_single_active(ob_base)
    ob_hd.select_set(True)
    bpy.ops.object.data_transfer( data_type='VGROUP_WEIGHTS',
                                  use_create=True,
                                  vert_mapping='NEAREST',
                                  use_object_transform=True,
                                  use_max_distance=False,  # use_max_distance=True, max_distance=0.1
                                  layers_select_src='ALL',
                                  layers_select_dst='NAME',
                                  mix_mode='REPLACE',
                                  mix_factor=1 )


def transfer_vertex_weights(ob_base, ob_hd, level, collection_name, smooth_iters=0):
    ob_base2 = copy_object(ob_base, collection_name=collection_name)
    ob_base2.location *= 0
    ob_hd.location *= 0
    assert( len(ob_hd.modifiers) == 0 )
    assert( ob_hd.data.shape_keys is None )
    # ~ ob_hd.modifiers.clear()

    apply_subsurf_modifier(ob_base2, level, predelete_all_modifiers=True) # this might not be necessary

    apply_data_transfer_operator(ob_base2, ob_hd)
    # ~ apply_data_transfer_modifier(ob_base2, ob_hd)
    delete_object(ob_base2)

    make_single_active(ob_hd)
    bpy.ops.object.vertex_group_clean(group_select_mode='ALL', limit=0.001, keep_single=True)
    # ~ bpy.ops.object.vertex_group_normalize_all(group_select_mode='ALL', lock_active=False)
    if smooth_iters > 0:
        bpy.context.object.data.use_paint_mask_vertex = False
        bpy.context.object.data.use_paint_mask = False
        bpy.ops.object.mode_set(mode='WEIGHT_PAINT', toggle=False)
        bpy.ops.object.vertex_group_smooth(group_select_mode='ALL', factor=0.5, repeat=smooth_iters, expand=0)
        bpy.ops.object.mode_set(mode='OBJECT', toggle=False)

    copy_armature(ob_base, ob_hd, collection_name)

def copy_armature(src, dst, collection_name=None, move_to_top=False):
    arm = get_armature(src)
    if arm is not None:
        make_single_active(arm)
        # ~ arm2 = copy_object(arm, collection_name)
        bpy.ops.object.duplicate(linked=False)
        arm2 = bpy.context.selected_objects[0]
        move_to_collection(arm2, collection_name)
        arm2.location = dst.location

        make_single_active(arm2)
        dst.select_set(True)
        bpy.ops.object.parent_set(type='ARMATURE')
        make_single_active(dst)
        for m in dst.modifiers:
            if m.type == 'ARMATURE':
                m.use_deform_preserve_volume = True
                if move_to_top:
                    bpy.ops.object.modifier_move_to_index(modifier=m.name, index=0)

def create_unsubdivide_multires(ob):
    if len(ob.modifiers) != 0 or ob.data.shape_keys is not None:
            raise RuntimeError("Object has modifiers or shape keys")
        # ~ ob.modifiers.clear()
        # ~ utils.remove_shape_keys(ob)

    mr = ob.modifiers.new("hd_multires", 'MULTIRES')

    mr.show_only_control_edges = True
    mr.quality = 4
    # ~ mr.subdivision_type = 'CATMULL_CLARK'
    mr.uv_smooth = 'PRESERVE_CORNERS'
    mr.boundary_smooth = 'ALL'
    mr.use_creases = True
    mr.use_custom_normals = False

    make_single_active(ob)
    bpy.ops.object.multires_rebuild_subdiv(modifier=mr.name)
    # ~ bpy.ops.object.multires_unsubdivide(modifier=mr.name)
    # ~ mr.levels = 0
    bpy.ops.object.select_all(action='DESELECT')


def create_subsurf_modifier(ob, use_limit=False):
    sd = ob.modifiers.new("dazHDmorph_subsurf", 'SUBSURF')
    sd.show_only_control_edges = True
    sd.use_limit_surface = use_limit
    sd.uv_smooth = 'PRESERVE_CORNERS'
    sd.boundary_smooth = 'ALL'
    sd.use_creases = True
    sd.use_custom_normals = False
    return sd

def create_multires_modifier(ob):
    mr = ob.modifiers.new("dazHDmorph_multires", 'MULTIRES')
    mr.show_only_control_edges = True
    mr.quality = 4
    # ~ mr.subdivision_type = 'CATMULL_CLARK'
    mr.uv_smooth = 'PRESERVE_CORNERS'
    mr.boundary_smooth = 'ALL'
    mr.use_creases = True
    mr.use_custom_normals = False
    return mr

def apply_subsurf_modifier(ob, level, predelete_all_modifiers=False):
    if level < 1: return
    if predelete_all_modifiers:
        ob.modifiers.clear()
    else:
        remove_division_modifiers(ob)
    remove_shape_keys(ob)

    sd = create_subsurf_modifier(ob)
    make_single_active(ob)
    bpy.ops.object.modifier_move_to_index(modifier=sd.name, index=0)
    sd.levels = level
    bpy.ops.object.modifier_apply(modifier=sd.name)


def apply_multires_modifier(ob, level, predelete_all_modifiers=False):
    if level < 1: return
    if predelete_all_modifiers:
        ob.modifiers.clear()
    else:
        remove_division_modifiers(ob)
    remove_shape_keys(ob)

    mr = create_multires_modifier(ob)
    make_single_active(ob)
    bpy.ops.object.modifier_move_to_index(modifier=mr.name, index=0)
    for _ in range(0, level):
        bpy.ops.object.multires_subdivide(modifier=mr.name, mode='CATMULL_CLARK')
    assert(mr.total_levels == level)
    mr.levels = level
    bpy.ops.object.modifier_apply(modifier=mr.name)


def get_material_prefix(mat_name):
    t = mat_name.rsplit(".", 1)
    if len(t) != 2 or not t[1].isdigit():
        return mat_name
    return t[0]

def merge_materials_by_prefix(src_ob, dst_ob):
    if len(src_ob.material_slots) == 0:
        return

    src_mats = {}
    for ms in src_ob.material_slots:
        ms_prefix = get_material_prefix(ms.name)
        src_mats[ms_prefix] = ms.material

    for ms in dst_ob.material_slots:
        ms_prefix = get_material_prefix(ms.name)
        if ms_prefix in src_mats:
            mat = ms.material
            ms.material = src_mats[ms_prefix]
            if mat.users == 0:
                bpy.data.materials.remove(mat, do_unlink=True)

def merge_materials_by_slot(src_ob, dst_ob):
    src_mats = {}
    for i, ms in enumerate(src_ob.material_slots):
        src_mats[i] = ms.material

    for ms in dst_ob.material_slots:
        if ms.name.startswith("SLOT_"):
            slot = ms.name[5:].strip().rsplit(".",1)[0]
            if slot.isdigit():
                slot = int(slot)
                if slot in src_mats:
                    mat = ms.material
                    ms.material = src_mats[slot]
                    #if mat.users == 0:
                    bpy.data.materials.remove(mat, do_unlink=True)

def get_subdivision_level(base_ob, hd_ob):
    # ~ nt = 0
    # ~ nq = 0
    # ~ for p in base_ob.data.polygons:
        # ~ nv = len(p.vertices)
        # ~ if nv == 4:
            # ~ nq += 1
        # ~ elif nv == 3:
            # ~ nt += 1

    # ~ nq2 = len(hd_ob.data.polygons)
    # ~ level = int( math.log(nq2/(4*nq + 3*nt)) / math.log(4) ) + 1

    # this only works right for mixtures of triangles and quads (no n-gons):
    nq = len(base_ob.data.polygons)
    nq2 = len(hd_ob.data.polygons)
    level = round( math.log(nq2/nq) / math.log(4) )

    return level


def remove_armature_modifiers(ob):
    for m in ob.modifiers:
        if m.type == 'ARMATURE':
            ob.modifiers.remove(m)

def remove_division_modifiers(ob):
    for m in ob.modifiers:
        if m.type in ('SUBSURF', 'MULTIRES'):
            ob.modifiers.remove(m)

def remove_shape_keys(ob):
    if ob.data.shape_keys and len(ob.data.shape_keys.key_blocks) > 0:
        ob.active_shape_key_index = 0
        ob.shape_key_clear()

def join_shapes(ob_src, ob_dst):
    make_single_active(ob_dst)
    ob_src.select_set(True)
    bpy.ops.object.join_shapes()

def delete_loose_edges(ob):
    make_single_active(ob)
    bpy.ops.object.mode_set(mode='EDIT', toggle=False)
    bpy.ops.mesh.delete_loose(use_verts=False, use_edges=True, use_faces=False)
    bpy.ops.object.mode_set(mode='OBJECT', toggle=False)

def fill_all_vgroups(ob):
    print("Filling vertex groups... ", end="", flush=True)
    t0 = time.perf_counter()
    make_single_active(ob)
    bpy.ops.object.mode_set(mode='EDIT', toggle=False)
    bpy.ops.mesh.select_all(action='SELECT')
    bpy.ops.object.vertex_group_invert(group_select_mode='ALL', auto_assign=True, auto_remove=False)
    # ~ bpy.ops.object.vertex_group_levels(group_select_mode='ALL', offset=0, gain=0)
    bpy.ops.object.mode_set(mode='OBJECT', toggle=False)
    print("fill_all_vgroups(): {0}".format(time.perf_counter() - t0))

def empty_all_vgroups(ob):
    # ~ print("Emptying vertex groups... ", end="", flush=True)
    # ~ t0 = time.perf_counter()
    make_single_active(ob)
    bpy.ops.object.mode_set(mode='EDIT', toggle=False)
    bpy.ops.object.vertex_group_remove_from(use_all_groups=True, use_all_verts=True)
    bpy.ops.object.mode_set(mode='OBJECT', toggle=False)
    # ~ print("empty_all_vgroups(): {0}".format(time.perf_counter() - t0))

def trim_vgroups(ob):
    print("Cleaning vertex groups... ", end="", flush=True)
    t0 = time.perf_counter()
    make_single_active(ob)
    bpy.ops.object.vertex_group_clean(group_select_mode='ALL', limit=0.001, keep_single=False)
    print("trim_vgroups(): {0}".format(time.perf_counter() - t0))

def new_shape_key(ob, name):
    if ob.data.shape_keys is None:
        ob.shape_key_add(name="Basis")
    ob.shape_key_add(name=name)

def get_shape_key_offset(ob):
    if ob.data.shape_keys is None:
        return 1
    return len(ob.data.shape_keys.key_blocks)

def create_ob_from_sk(src_ob, skn, delete_src_sk=False):
    assert(src_ob.data.shape_keys is not None)
    sks = src_ob.data.shape_keys.key_blocks
    assert(skn < len(sks))
    src_ob2 = copy_object(src_ob)
    for i, sk in enumerate(src_ob2.data.shape_keys.key_blocks):
        if i != skn:
            src_ob2.shape_key_remove(sk)
    src_ob2.shape_key_clear()
    if delete_src_sk:
        src_ob.shape_key_remove(sks[skn])
    return src_ob2

def read_dhdm_level(dhdm_fp):
    if not os.path.isfile(dhdm_fp):
        raise ValueError("File \"{}\" not found.".format(dhdm_fp))
    with open(dhdm_fp, "rb") as f:
        h = f.read(8)
    return int.from_bytes(h[4:], byteorder='little', signed=False)

def is_gzip_file(fp):
    with open(fp, "rb") as f:
        return f.read(2) == b'\x1f\x8b'

def get_dsf_json(dsf_fp):
    j = None
    if is_gzip_file(dsf_fp):
        with gzip.open(dsf_fp, "rt", encoding="utf-8") as f:
            j = json.loads(f.read())
    else:
        with open(dsf_fp, "r", encoding="utf-8") as f:
            j = json.loads(f.read())
    return j

def read_dsf_level(dsf_fp):
    j = get_dsf_json(dsf_fp)
    try:
        dhdm = j["modifier_library"][0]["morph"]["hd_url"]
        dhdm_fp = os.path.join( os.path.dirname(dsf_fp), os.path.basename(dhdm) )
        return read_dhdm_level(dhdm_fp)
    except KeyError:
        return 0

def read_dsf_id(dsf_fp, only_with_dhdm=False):
    j = get_dsf_json(dsf_fp)
    try:
        if only_with_dhdm:
            dhdm = j["modifier_library"][0]["morph"]["hd_url"]
        return j["modifier_library"][0]["id"]
    except KeyError:
        return None

def dsf_vcount(dsf_fp):
    j = get_dsf_json(dsf_fp)
    try:
        v_count = j["modifier_library"][0]["morph"]["vertex_count"]
        return v_count
    except KeyError:
        return None

def geograft_file_vcounts(geograft_file):
    with open(geograft_file, "r", encoding="utf-8") as f:
        d = json.load(f)
        return set([int(k) for k in d])

def get_morph_groups_levels(morph_groups, max_subd):
    tot_groups = 0
    groups_levels = {}
    for g, tuples in morph_groups.items():
        level = 0
        for t in tuples:
            fp = t[0]
            if has_extension(fp, "dhdm"):
                level = max(level, read_dhdm_level(fp))
            else:
                level = max(level, read_dsf_level(fp))
        if max_subd > 0:
            level = min(level, max_subd)
        if level == 0:
            continue
        if level not in groups_levels:
            groups_levels[level] = []
        groups_levels[level].append(g)
        tot_groups += 1

    groups_by_level = []
    for level in sorted(groups_levels.keys()):
        groups_by_level.append( (level, groups_levels[level]) )
    return groups_by_level, tot_groups

def get_file_id(filepath):
    filename = os.path.basename(filepath).rsplit(".",1)[0]

    if has_extension(filepath, "dhdm"):
        dsf_fp = os.path.join(os.path.dirname(filepath), filename + ".dsf")
        if not os.path.isfile(dsf_fp):
            return filename
        filepath = dsf_fp

    assert(has_extension(filepath, "dsf"))
    dsf_id = read_dsf_id(filepath)
    if dsf_id is not None:
        return dsf_id
    return filename

def get_sk_names(ob):
    if ob.data.shape_keys is None:
        return []
    sks = []
    for sk in ob.data.shape_keys.key_blocks[1:]:
        sks.append(sk.name)
    return sks

def get_sk_pairs(src_ob, dst_ob):
    dst_sks = get_sk_names(dst_ob)
    src_sks = get_sk_names(src_ob)
    if len(src_sks) == 0 or len(dst_sks) == 0:
        return {}
    src_sks_map = {}
    for sk_name in src_sks:
        src_sks_map[sk_name.lower()] = sk_name
    pairs = {}
    for sk_name in dst_sks:
        sk_name2 = sk_name.lower()
        src_sk_name = src_sks_map.get(sk_name2)
        if src_sk_name is not None:
            pairs[src_sk_name] = sk_name
    return pairs

def copy_sk_drivers(src_ob, dst_ob, sks_pairs):
    def get_sk_data_path(sk_name):
        return "key_blocks[\"{0}\"].value".format(sk_name)

    if len(sks_pairs) == 0:
        return

    src_armat = get_armature(src_ob)
    dst_armat = get_armature(dst_ob)
    if src_armat is None or dst_armat is None:
        return

    src_ob_sk = src_ob.data.shape_keys
    dst_ob_sk = dst_ob.data.shape_keys
    if src_ob_sk is None or src_ob_sk.animation_data is None:
        return
    if dst_ob_sk is None:
        return

    dst_fcu_data_paths = set()
    if dst_ob_sk.animation_data is None:
        dst_ob_sk.animation_data_create()
    else:
        for fcu in dst_ob_sk.animation_data.drivers:
            dst_fcu_data_paths.add(fcu.data_path)

    sk_data_paths = {}
    for src_sk_name, dst_sk_name in sks_pairs.items():
        src_sk_dp = get_sk_data_path(src_sk_name)
        sk_data_paths[src_sk_dp] = dst_sk_name

    for fcu in src_ob_sk.animation_data.drivers:
        if fcu.data_path in sk_data_paths:
            dst_fcu_data_path = get_sk_data_path(sk_data_paths[fcu.data_path])
            if dst_fcu_data_path in dst_fcu_data_paths:
                continue
            dst_fcu = dst_ob_sk.animation_data.drivers.from_existing(src_driver=fcu)
            dst_fcu.data_path = dst_fcu_data_path
            for v in dst_fcu.driver.variables:
                for trg in v.targets:
                    if trg.id_type == 'OBJECT' and trg.id == src_armat:
                        trg.id = dst_armat
                    elif trg.id_type == 'ARMATURE' and trg.id == src_armat.data:
                        trg.id = dst_armat.data

def copy_uv_layers_names(base_ob, hd_ob):
    for i, layer in enumerate(base_ob.data.uv_layers):
        if i >= len(hd_ob.data.uv_layers):
            return
        hd_ob.data.uv_layers[i].name = layer.name

def remove_non_active_uvs(hd_ob):
    if len(hd_ob.data.uv_layers) == 0:
        return
    layers_to_remove = [layer for layer in hd_ob.data.uv_layers if not layer.active]
    for layer in layers_to_remove:
        hd_ob.data.uv_layers.remove(layer)

def create_subd_vgroups(p, ob):
    bone_verts = []
    for i in range(0, p.tot_bones):
        tmp = []
        for j in range(0, p.tot_bone_verts[i]):
            tmp.append( p.bone_verts[i][j] )
        bone_verts.append(tmp)

    for i, vg in enumerate(ob.vertex_groups):
        vg.add(bone_verts[i], 0.0, "REPLACE")

def get_selected_meshes(context):
    return [ob for ob in context.view_layer.objects
            if ob.select_get() and ob.type == 'MESH' and not (ob.hide_get() or ob.hide_viewport)]

def delete_property(rna, p):
    att = getattr(rna, p, None)
    if att is None:
        return
    if isinstance(att, bpy.types.bpy_prop_collection):
        att.clear()
    elif isinstance(att, str):
        rna[p] = ""
    elif isinstance(att, int):
        rna[p] = 0
    elif isinstance(att, bool):
        rna[p] = False

def remove_daz_properties(ob):
    if ob.type != 'MESH':
        return

    ob_properties = ( "DazMultires", )

    mesh_properties = ( "DazRigidityGroups", "DazOrigVerts",
                        "DazGraftGroup", "DazMaskGroup",
                        "DazMatNums", "DazMaterialSets",
                        "DazHDMaterials",

                        "DazFingerPrint", "DazVertexCount",
                        "DazHairType" )

    for p in ob_properties:
        delete_property(ob, p)
    for p in mesh_properties:
        delete_property(ob.data, p)

def set_active_uv_layer(ob, n):
    if n >= len(ob.data.uv_layers):
        raise ValueError("Object \"{}\" has no uv layer with index {}".format(ob.name, n))
    ob.data.uv_layers.active_index = n

def delete_uv_layers(ob, preserve=[]):
    preserve = set(preserve)
    to_remove = []
    for i, layer in enumerate(ob.data.uv_layers):
        if i not in preserve:
            to_remove.append(layer)
    for layer in to_remove:
        ob.data.uv_layers.remove(layer)
