import bpy, os, json, time
from collections.abc import Mapping
from . import dll_wrapper
from . import utils
from .operator_common import HDMorphOperator


class UDIM_Baker:
    bake_types = { 'NORMAL':    {"prefix": 'NM',     "setting": 'NORMAL'},
                   'MR_NORMAL': {"prefix": "mrNM",   "setting": 'NORMALS'},
                   'MR_DISP':   {"prefix": "mrDISP", "setting": 'DISPLACEMENT'},
                 }

    image_size = {"512", "1024", "2048", "4096"}

    image_types = {'PNG8', 'PNG16', 'EXR32'}

    def __init__(self):
        self.output_dir = None
        self.bakeType = None
        self.unsubdivide_times = None
        self.tiles_to_bake = None

        self.image_basename = None
        self.imageSize = None
        self.imageType = None
        self.use_float_buffer = None
        self.image_ext = None

        self.use_multires = None
        self.img_float_scene = None
        self.saved_bake_settings = None
        self.saved_materials = None
        self.saved_materials_indices = None
        self.last_tilesSet = None
        self.uv_layer = None

    def setTilesToBake(self, tiles):
        if tiles is None:
            self.tiles_to_bake = None
        else:
            self.tiles_to_bake = set(tiles)

    def setUVLayer(self, layer_n):
        self.uv_layer = layer_n

    def setBakeType(self, bake_type):
        if bake_type not in self.bake_types:
            raise ValueError("Invalid bakeType: \"{}\".".format(bake_type))
        self.bakeType = bake_type
        self.use_multires = bool(bake_type.startswith("MR_"))

    def setImageSize(self, image_size):
        if image_size not in self.image_size:
            raise ValueError("Invalid imageSize")
        self.imageSize = image_size

    def setImageType(self, image_type):
        if image_type not in self.image_types:
            raise ValueError("Invalid imageType")
        self.imageType = image_type
        self.use_float_buffer = ( self.imageType in ('PNG16', 'EXR32') )
        if self.imageType != 'EXR32':
            self.image_ext = "png"
        else:
            self.image_ext = "exr"

    def setOutputDirpath(self, output_dir):
        if self.bakeType is None:
            raise RuntimeError("bakeType not set")
        self.output_dir = os.path.join(output_dir, self.bakeType)
        if not os.path.exists(self.output_dir):
            os.mkdir(self.output_dir)
        return self.output_dir

    def setMultiRes(self, ob):
        if len(ob.modifiers) != 0 or ob.data.shape_keys is not None:
            raise RuntimeError("Object has modifiers or shape keys")
        # ~ ob.modifiers.clear()
        # ~ utils.remove_shape_keys(ob)

        mr = ob.modifiers.new("dazHDmorph_multires_bake", 'MULTIRES')

        mr.show_only_control_edges = True
        mr.quality = 4
        # ~ mr.subdivision_type = 'CATMULL_CLARK'
        mr.uv_smooth = 'PRESERVE_CORNERS'
        mr.boundary_smooth = 'ALL'
        mr.use_creases = True
        mr.use_custom_normals = False

        utils.make_single_active(ob)
        if self.unsubdivide_times < 0:
            bpy.ops.object.multires_rebuild_subdiv(modifier=mr.name)
        else:
            for _ in range(0, self.unsubdivide_times):
                bpy.ops.object.multires_unsubdivide(modifier=mr.name)
        mr.levels = 0
        bpy.ops.object.select_all(action='DESELECT')

    def get_multires_bake_settings(self):
        bake_settings = {}

        bake_settings["render"] = {}
        bake_settings["render"]["engine"] = 'CYCLES'
        bake_settings["render"]["use_bake_multires"] = True
        bake_settings["render"]["bake_type"] = self.bake_types[self.bakeType]["setting"]
        bake_settings["render"]["bake_margin"] = 2
        bake_settings["render"]["use_bake_clear"] = True
        bake_settings["render"]["use_bake_lores_mesh"] = False

        bake_settings["cycles"] = {}
        bake_settings["cycles"]["samples"] = 1    # 512

        return bake_settings

    def get_bake_settings(self):
        bake_settings = {}

        bake_settings["render"] = {}
        bake_settings["render"]["engine"] = 'CYCLES'
        bake_settings["render"]["use_bake_multires"] = False

        bake_settings["render"]["bake"] = {}
        bake_settings["render"]["bake"]["normal_r"] = 'POS_X'
        bake_settings["render"]["bake"]["normal_g"] = 'POS_Y'
        bake_settings["render"]["bake"]["normal_b"] = 'POS_Z'

        bake_settings["render"]["bake"]["use_selected_to_active"] = True
        bake_settings["render"]["bake"]["use_cage"] = False
        bake_settings["render"]["bake"]["cage_extrusion"] = 0.004
        bake_settings["render"]["bake"]["max_ray_distance"] = 0.0
        bake_settings["render"]["bake"]["margin"] = 2
        bake_settings["render"]["bake"]["use_clear"] = True

        bake_settings["cycles"] = {}
        bake_settings["cycles"]["samples"] = 1    # 512
        bake_settings["cycles"]["bake_type"] = self.bake_types[self.bakeType]["setting"]

        return bake_settings

    @classmethod
    def unwind_settings(cls, d, obj, d2):
        for k, v in d.items():
            if isinstance(v, Mapping):
                d2[k] = {}
                obj2 = getattr(obj, k)
                cls.unwind_settings(v, obj2, d2[k])
            else:
                d2[k] = getattr(obj, k)
                setattr(obj, k, v)

    def setSceneSettings(self):
        self.restoreSceneSettings()
        if self.bakeType is None or self.use_multires is None:
            raise RuntimeError("Call to setSceneSettings() with uninitialized 'bakeType' and 'use_multires'")

        scn = bpy.context.scene
        self.saved_bake_settings = {}
        if not self.use_multires:
            bake_settings = self.get_bake_settings()
        else:
            bake_settings = self.get_multires_bake_settings()
        self.unwind_settings(bake_settings, scn, self.saved_bake_settings)

        if self.use_float_buffer:
            scn = bpy.data.scenes.new('img_float_settings')
            scn.render.use_file_extension = True
            scn.render.use_render_cache = False
            if self.imageType == 'PNG16':
                scn.render.image_settings.file_format = 'PNG'
                scn.render.image_settings.color_mode = 'RGB'
                scn.render.image_settings.color_depth = '16'
                scn.render.image_settings.compression = 15
            elif self.imageType == 'EXR32':
                scn.render.image_settings.file_format = 'OPEN_EXR'
                scn.render.image_settings.color_mode = 'RGB'
                scn.render.image_settings.color_depth = '32'
                scn.render.image_settings.exr_codec = 'ZIP'
                scn.render.image_settings.use_zbuffer = False
                scn.render.image_settings.use_preview = False
            else:
                raise ValueError("Invalid imageType")
            scn.render.use_overwrite = True
            scn.render.use_placeholder = False
            self.img_float_scene = scn

    def restoreSceneSettings(self):
        if self.saved_bake_settings is not None:
            scn = bpy.context.scene
            self.unwind_settings(self.saved_bake_settings, scn, {})
            self.saved_bake_settings = None

        if self.img_float_scene is not None:
            bpy.data.scenes.remove(self.img_float_scene, do_unlink=True)
            self.img_float_scene = None

    def storeMaterials(self, ob):
        self.saved_materials_indices = [f.material_index for f in ob.data.polygons]
        self.saved_materials = list(ob.data.materials)
        for _ in self.saved_materials:
            ob.data.materials.pop()

    def restoreMaterials(self, ob):
        if self.saved_materials is None or self.saved_materials_indices is None:
            raise RuntimeError("Call to restoreMaterials() with no saved materials")
        for _ in list(ob.data.materials):
            ob.data.materials.pop()
        for mat in self.saved_materials:
            ob.data.materials.append(mat)
        for fi, mi in enumerate(self.saved_materials_indices):
            ob.data.polygons[fi].material_index = mi
        self.saved_materials = None
        self.saved_materials_indices = None

    def makeImage(self, tile):
        image_filename = "{0}-{1}-{2}_{3}.{4}".format( self.image_basename,
                                                       self.bake_types[self.bakeType]["prefix"],
                                                       self.imageSize, tile, self.image_ext )
        size = int(self.imageSize)
        img = bpy.data.images.new( image_filename, size, size,
                                   alpha=False, is_data=True,
                                   float_buffer=self.use_float_buffer )

        img.colorspace_settings.name = 'Non-Color'
        img.file_format = 'PNG'
        img.filepath = os.path.join(self.output_dir, image_filename)
        return img

    @staticmethod
    def makeMaterial(ob, img):
        mat = bpy.data.materials.new(img.name)
        ob.data.materials.append(mat)
        ob.active_material = mat
        mat.use_nodes = True
        tree = mat.node_tree
        tree.nodes.clear()
        texco = tree.nodes.new(type = "ShaderNodeTexCoord")
        texco.location = (0, 0)
        node = tree.nodes.new(type = "ShaderNodeTexImage")
        node.location = (200,0)
        node.image = img
        node.extension = 'CLIP'
        node.select = True
        tree.nodes.active = node
        tree.links.new(texco.outputs["UV"], node.inputs["Vector"])
        return mat

    @staticmethod
    def get_face_tile(f, uvloop):
        n = len(f.vertices)
        rx = sum( [uvloop.data[k].uv[0] for k in f.loop_indices] ) / n
        ry = sum( [uvloop.data[k].uv[1] for k in f.loop_indices] ) / n
        i = max(0, int(round(rx-0.5)))
        j = max(0, int(round(ry-0.5)))
        tile = 1001 + 10 * j + i
        return tile

    def getTiles(self, ob):
        uvloop = ob.data.uv_layers[self.uv_layer]
        tiles = {}
        for f in ob.data.polygons:
            t = self.get_face_tile(f, uvloop)
            if t not in tiles:
                tiles[t] = []
            tiles[t].append(f.index)
        return tiles

    def getTilesSet(self, ob):
        uvloop = ob.data.uv_layers[self.uv_layer]
        tiles = set()
        for f in ob.data.polygons:
            tiles.add(self.get_face_tile(f, uvloop))
        return tiles

    def translateTile(self, ob, tile, sign):
        j = ( tile - 1001 ) // 10
        i = ( tile - 1001 - 10*j ) % 10
        dx = sign*i
        dy = sign*j
        uvloop = ob.data.uv_layers[self.uv_layer]
        for f in ob.data.polygons:
            for n in f.loop_indices:
                uvloop.data[n].uv[0] += dx
                uvloop.data[n].uv[1] += dy

    def bake(self, base_ob, hd_ob, preserve_materials, reuse_tilesSet):
        if not self.use_multires:
            baked_ob = base_ob
            utils.startProgress("Baking \"{0}\" onto \"{1}\".".format(hd_ob.name, base_ob.name))
        else:
            baked_ob = hd_ob
            self.setMultiRes(hd_ob)
            utils.startProgress("Baking \"{0}\" with multires.".format(hd_ob.name))

        if preserve_materials:
            self.storeMaterials(baked_ob)
        else:
            utils.delete_object_materials(baked_ob)

        tilesSet = None
        if reuse_tilesSet and self.last_tilesSet is not None:
            tilesSet = self.last_tilesSet
        else:
            tilesSet = self.getTilesSet(baked_ob)
            if reuse_tilesSet:
                self.last_tilesSet = tilesSet
        ntiles = len(tilesSet)

        saved_imgs = []
        for i, tile_n in enumerate(tilesSet):
            utils.showProgress(i, ntiles)
            if (self.tiles_to_bake is not None) and (tile_n not in self.tiles_to_bake):
                continue

            img = self.makeImage(tile_n)
            mat = self.makeMaterial(baked_ob, img)
            self.translateTile(baked_ob, tile_n, -1)

            utils.make_single_active(baked_ob)

            if not self.use_multires:
                hd_ob.select_set(True)
                bpy.ops.object.bake(type=self.bake_types[self.bakeType]["setting"])
            else:
                bpy.ops.object.bake_image()

            if self.use_float_buffer:
                img.save_render(img.filepath, scene=self.img_float_scene)
            else:
                img.save()

            print("Saved \"{0}\".".format(img.filepath))
            saved_imgs.append(img.filepath)
            self.translateTile(baked_ob, tile_n, 1)
            bpy.data.images.remove(img, do_unlink=True)
            baked_ob.data.materials.pop()
            bpy.data.materials.remove(mat, do_unlink=True)

        utils.showProgress(ntiles, ntiles)
        utils.endProgress()
        if preserve_materials:
            self.restoreMaterials(baked_ob)
        return saved_imgs

class GenerateNormalsCommon(HDMorphOperator):
    max_subd = None
    base_modifiers = None
    morph_base_until = None
    bake_type = None
    texture_size = None
    image_type = None
    uv_layer = None
    json_filename = "Morphs_NM-DISP.json"

    morphs_groups_filenames = None
    output_dirpath = None

    def generate_normals_json(self, outputDirpath, groups_textures):
        json_fp = os.path.join(outputDirpath, self.json_filename)
        if os.path.isfile(json_fp):
            with open(json_fp, "r", encoding="utf-8") as f:
                d = json.load(f)
        else:
            d = {}

        for g, morphs in self.morphs_groups.items():
            if g not in groups_textures:
                continue
            g_entry = {}
            g_entry["filepaths"] = [t[0] for t in morphs]
            g_entry["weights"] = [t[1] for t in morphs]
            g_entry["textures"] = groups_textures[g]
            d[ self.morphs_groups_filenames[g] ] = g_entry

        with open(json_fp, "w", encoding="utf-8") as f:
            json.dump(d, f, indent=4)
        return True

    def generate_maps(self, context):
        is_mr_bake = self.bake_type.startswith("MR_")
        uv_layer_override = None
        uv_layers = None
        if is_mr_bake:
            uv_layer_override = self.uv_layer + 1
            uv_layers = [self.uv_layer]
        else:
            uv_layer_override = self.uv_layer + 1
            uv_layers = [self.uv_layer]

            # ~ uv_layer_override = -1    # uvs in hd ob aren't needed
            # ~ uv_layers = []

        utils.set_active_uv_layer(self.base_ob, self.uv_layer)
        base_pointer = self.evaluated_base_pointer(context, self.base_modifiers)
        morph_groups_levels, tot_groups = utils.get_morph_groups_levels(self.morphs_groups, self.max_subd)
        ob_output = self.create_output_ob(0, clear_vgroups=True, uv_layers=uv_layers)

        ob_output_level = 0
        skn = -1 if (tot_groups == 1) else 0

        ub = UDIM_Baker()
        ub.setBakeType(self.bake_type)
        ub.setImageSize(self.texture_size)
        ub.setImageType(self.image_type)
        ub.setTilesToBake(self.tiles)
        if is_mr_bake:
            ub.setUVLayer(0)    # in copied object
            utils.set_active_uv_layer(ob_output, 0)
        else:
            ub.setUVLayer(self.uv_layer)  # in base object
            ub.storeMaterials(self.base_ob)

        texOutputDirpath = ub.setOutputDirpath( self.output_dirpath )
        ub.setSceneSettings()

        try:
            groups_textures = {}
            for level, groups in morph_groups_levels:
                morphs_groups2 = {}
                morphs_groups_filenames2 = {}

                for g in groups:
                    morphs_groups2[g] = self.morphs_groups[g]
                    morphs_groups_filenames2[g] = self.morphs_groups_filenames[g]

                assert(level > ob_output_level)
                utils.apply_subsurf_modifier(ob_output, level - ob_output_level, predelete_all_modifiers=True)
                ob_output_level = level
                if skn == 0:
                    for _ in range(0, len(morphs_groups2)):
                        utils.new_shape_key(ob_output, "tmp")

                dll_wrapper.execute_in_new_thread( "generate_shapes",
                                                   self.gScale, base_pointer, ob_output.as_pointer(),
                                                   level, self.geograft_file,
                                                   morphs_groups2,
                                                   skn, False,
                                                   uv_layer_override )

                self.clean_output_ob(ob_output)
                # ~ context.view_layer.depsgraph.update()

                for g in morphs_groups2:
                    # ~ morph_name = morphs_groups_filenames2[g].rsplit(".", 1)[0]
                    morph_name = morphs_groups_filenames2[g]
                    print("\n--Baking of morph group \"{0}\":".format(morph_name))

                    if skn == 0:
                        morph_ob = utils.create_ob_from_sk(ob_output, 1, delete_src_sk=True)
                    else:
                        morph_ob = ob_output

                    morph_levels = utils.get_subdivision_level(self.base_ob, morph_ob)
                    unsubdivide_times = -1
                    if self.morph_base_until > 0:
                        unsubdivide_times = morph_levels - self.morph_base_until
                        if unsubdivide_times <= 0:
                            self.report({'ERROR'}, "Morph max level is smaller or equal than \"morph base until\".")
                            return False

                    utils.make_single_active(morph_ob)
                    ub.image_basename = morph_name
                    ub.unsubdivide_times = unsubdivide_times
                    saved_imgs = ub.bake(self.base_ob, morph_ob, preserve_materials=False, reuse_tilesSet=True)
                    groups_textures[g] = saved_imgs
                    # ~ return True
                    utils.delete_object(morph_ob)

        except Exception as e:
            raise e
        finally:
            if not is_mr_bake:
                ub.restoreMaterials(self.base_ob)
            ub.restoreSceneSettings()

        self.generate_normals_json(texOutputDirpath, groups_textures)
        if skn == 0:
            utils.delete_object(ob_output)
        return True

class GenerateNormalsOperator(GenerateNormalsCommon):
    """Generate baked normal/grayscale displacement textures from morph/s"""
    bl_idname = "dazhdmorph.normals"
    bl_label = "Generate baked textures"

    def execute(self, context):
        if not self.check_input(context):
            return {'CANCELLED'}
        if not self.check_morphs_list(context):
            return {'CANCELLED'}
        if not self.check_texture_tiles(context):
            return {'CANCELLED'}

        addon_props = context.scene.daz_hd_morph
        self.max_subd = addon_props.texture_max_subd
        self.base_modifiers = addon_props.base_modifiers
        self.morph_base_until = addon_props.morph_base_until
        self.bake_type = addon_props.normal_bake_type
        self.texture_size = addon_props.texture_size
        self.image_type = addon_props.normal_image_type
        self.uv_layer = addon_props.texture_uv_layer
        if not self.check_uv_layer(self.uv_layer):
            return {'CANCELLED'}

        t0 = time.perf_counter()
        if not self.setup(context):
            return {'CANCELLED'}

        r = self.generate_maps(context)
        self.cleanup(context)
        if not r:
            return {'CANCELLED'}

        self.report({'INFO'}, "Normal maps generated.")
        print("Elapsed: {}".format(time.perf_counter() - t0))

        return {'FINISHED'}

    def setup(self, context):
        self.morphs_groups_filenames = utils.getMorphsGroupsFilenames(self.morphs_groups, suffix="")
        self.output_dirpath = self.create_normal_subdir()
        return True

class GenerateNormalsFacsOperator(GenerateNormalsCommon):
    """Generate normal textures for facs shape keys currently on base mesh"""
    bl_idname = "dazhdmorph.normals_facs"
    bl_label = "Generate facs normals"

    facs_dir = None

    def execute(self, context):
        if not self.check_input(context):
            return {'CANCELLED'}
        if not self.check_facs(context):
            return {'CANCELLED'}
        if not self.check_texture_tiles(context):
            return {'CANCELLED'}

        addon_props = context.scene.daz_hd_morph
        self.max_subd = addon_props.texture_max_subd
        self.base_modifiers = addon_props.base_modifiers
        self.morph_base_until = addon_props.morph_base_until
        self.bake_type = addon_props.normal_bake_type
        self.texture_size = addon_props.texture_size
        self.image_type = addon_props.normal_image_type
        self.uv_layer = addon_props.texture_uv_layer
        if not self.check_uv_layer(self.uv_layer):
            return {'CANCELLED'}

        t0 = time.perf_counter()
        if not self.setup(context):
            return {'CANCELLED'}

        r = self.generate_maps(context)
        self.cleanup(context)
        if not r:
            return {'CANCELLED'}

        self.report({'INFO'}, "facs normals generated.")
        print("Elapsed: {}".format(time.perf_counter() - t0))

        return {'FINISHED'}

    def check_facs(self, context):
        try:
            from import_daz import api
            morphs_dirs = api.get_morph_directories(self.base_ob)
        except Exception as e:
            # ~ raise e
            self.report({'ERROR'}, "Required addon \"import_daz\" not found, or api function threw an exception.")
            return False

        for d in morphs_dirs:
            if os.path.basename(d).strip().lower() == 'facs' and os.path.isdir(d):
                self.facs_dir = d
                return True
        self.report({'ERROR'}, "Base mesh has no facs loaded.")
        return False

    def setup(self, context):
        base_sks = utils.get_sk_names(self.base_ob)
        if len(base_sks) == 0:
            return False
        base_sks_map = {}
        for k in base_sks:
            base_sks_map[k.lower()] = k
        del base_sks

        base_hd_facs = []
        for root, dirs, files in os.walk(self.facs_dir):
            for f in files:
                if utils.has_extension(f, "dsf"):
                    fp = os.path.join(root, f)
                    dsf_id = utils.read_dsf_id(fp, only_with_dhdm=True)
                    if dsf_id is not None:
                        src_sk_name = base_sks_map.get( dsf_id.lower(), None )

                        if not src_sk_name.startswith("facs_bs_Mouth") or "Left" not in src_sk_name:
                            continue

                        if src_sk_name is not None:
                            base_hd_facs.append( (src_sk_name, fp) )
            break

        if len(base_hd_facs) == 0:
            self.report({'ERROR'}, "Found no base HD facs shape keys.")
            return False
        else:
            print("- Found {0} base HD facs shape keys. Generating textures...".format(len(base_hd_facs)))

        morphs_groups = {}
        morphs_groups_filenames = {}
        for i, t in enumerate(base_hd_facs):
            morphs_groups[i] = []
            morphs_groups[i].append( (t[1], 1) )
            morphs_groups_filenames[i] = t[0]

        self.morphs_groups, self.morphs_groups_filenames = morphs_groups, morphs_groups_filenames
        normals_dir = self.create_normal_subdir()
        facs_normals_dir = os.path.join(normals_dir, "facs")
        if not os.path.isdir(facs_normals_dir):
            os.mkdir(facs_normals_dir)
        self.output_dirpath = facs_normals_dir
        return True
