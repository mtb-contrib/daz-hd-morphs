import os, ctypes, concurrent.futures
from . import utils


def str_2_char_p(string):
    if string is None:
        return ctypes.c_char_p(None)
    return ctypes.c_char_p(bytes(string, encoding="utf-8"))

def str_2_char(string):
    return ctypes.c_char(bytes(string[0], encoding="utf-8"))

class MorphsInfo(ctypes.Structure):
    _fields_ = [ ( "groups_n", ctypes.c_ushort ),
                 ( "groups_counts", ctypes.POINTER(ctypes.c_ushort) ),
                 ( "morphs_filepaths", ctypes.POINTER( ctypes.POINTER(ctypes.c_char_p) ) ),
                 ( "morphs_weights", ctypes.POINTER( ctypes.POINTER(ctypes.c_float) ) ),
                 ( "morphs_types", ctypes.POINTER( ctypes.POINTER(ctypes.c_char_p) ) ),
                 ( "groups_basenames", ctypes.POINTER(ctypes.c_char_p) ) ]

    def __init__(self, morphs_groups, morphs_groups_basenames):
        if morphs_groups is None:
            groups_n = 0
            groups_counts = None
            morphs_filepaths = None
            morphs_weights = None
            morphs_types = None
            groups_basenames = None
        else:
            groups_n = len(morphs_groups)

            groups_counts = (ctypes.c_ushort * groups_n)()
            groups_basenames = None
            if morphs_groups_basenames is not None:
                groups_basenames = (ctypes.c_char_p * groups_n)()
            morphs_filepaths = (ctypes.POINTER(ctypes.c_char_p) * groups_n)()
            morphs_weights = (ctypes.POINTER(ctypes.c_float) * groups_n)()
            morphs_types = (ctypes.POINTER(ctypes.c_char_p) * groups_n)()

            i = 0
            for g, morphs_list in morphs_groups.items():
                g_morphs_count = len(morphs_list)
                groups_counts[i] = ctypes.c_ushort(g_morphs_count)

                if morphs_groups_basenames is not None:
                    groups_basenames[i] = str_2_char_p( morphs_groups_basenames[g] )
                morphs_filepaths[i] = (ctypes.c_char_p * g_morphs_count)()
                morphs_weights[i] = (ctypes.c_float * g_morphs_count)()
                morphs_types[i] = (ctypes.c_char_p * g_morphs_count)()

                j = 0
                for fp, w, mt in morphs_list:
                    morphs_filepaths[i][j] = str_2_char_p(fp)
                    morphs_weights[i][j] = ctypes.c_float(w)
                    morphs_types[i][j] = str_2_char_p(mt)
                    j += 1
                i += 1

        self.groups_n = ctypes.c_ushort(groups_n)
        self.groups_counts = ctypes.cast(groups_counts, ctypes.POINTER(ctypes.c_ushort) )
        self.morphs_filepaths = ctypes.cast(morphs_filepaths, ctypes.POINTER( ctypes.POINTER(ctypes.c_char_p) ) )
        self.morphs_weights = ctypes.cast(morphs_weights, ctypes.POINTER( ctypes.POINTER(ctypes.c_float) ) )
        self.morphs_types = ctypes.cast(morphs_types, ctypes.POINTER( ctypes.POINTER(ctypes.c_char_p) ) )
        self.groups_basenames = ctypes.cast(groups_basenames, ctypes.POINTER(ctypes.c_char_p) )


class MeshInfo(ctypes.Structure):
    _fields_ = [ ("gScale", ctypes.c_float ),
                 ("baseMeshPointer", ctypes.c_void_p),
                 ("geograft_file", ctypes.c_char_p),
                 ("hd_level", ctypes.c_short),
                 ("hdMeshPointer", ctypes.c_void_p) ]

    def __init__( self, gScale, baseMeshPointer,
                  geograft_file=None, hdMeshPointer=None,
                  hd_level = 0 ):
        self.gScale = ctypes.c_float(gScale)
        self.baseMeshPointer = ctypes.c_void_p(baseMeshPointer)
        self.geograft_file = str_2_char_p(geograft_file)
        self.hd_level = ctypes.c_short(hd_level)
        self.hdMeshPointer = ctypes.c_void_p(hdMeshPointer)


class TextureInfo(ctypes.Structure):
    _fields_ = [  ("target_space", ctypes.c_char),
                  ("texture_size", ctypes.c_ushort),
                  ("image_type", ctypes.c_char),
                  ("auto_settings", ctypes.c_bool),
                  ("scale", ctypes.c_float),
                  ("midlevel", ctypes.c_float),
                  ("tiles_count", ctypes.c_ushort),
                  ("tiles", ctypes.POINTER(ctypes.c_ushort) ),
                  ("uv_layer", ctypes.c_short),
                  ("max_subd", ctypes.c_ushort),
                  ("output_dirpath", ctypes.c_char_p) ]

    def __init__(self, vdisp_ts, output_dirpath):
        self.target_space = str_2_char(vdisp_ts.target_space)
        self.texture_size = ctypes.c_ushort(vdisp_ts.texture_size)
        self.image_type = str_2_char(vdisp_ts.image_type)
        self.auto_settings = ctypes.c_bool(vdisp_ts.auto_settings)
        self.scale = ctypes.c_float(vdisp_ts.scale)
        self.midlevel = ctypes.c_float(vdisp_ts.midlevel)
        self.uv_layer = ctypes.c_short(vdisp_ts.uv_layer+1)
        self.max_subd = ctypes.c_ushort(vdisp_ts.max_subd)
        self.output_dirpath = str_2_char_p(output_dirpath)

        if vdisp_ts.tiles is None:
            self.tiles_count = ctypes.c_ushort(0)
            self.tiles = ctypes.POINTER(ctypes.c_ushort)()
        else:
            count = len(vdisp_ts.tiles)
            tiles_array =  (ctypes.c_ushort * count)()
            for i, t in enumerate(vdisp_ts.tiles):
                tiles_array[i] = ctypes.c_ushort(t)
            self.tiles_count = ctypes.c_ushort(count)
            self.tiles = ctypes.cast( tiles_array, ctypes.POINTER(ctypes.c_ushort) )


class BoneWeightsInfo(ctypes.Structure):
    _fields_ = [ ( "loadedBaseMeshPointer", ctypes.c_void_p ),
                 ( "tot_bones", ctypes.c_uint ),
                 ( "tot_bone_verts", ctypes.POINTER(ctypes.c_uint) ),
                 ( "bone_verts", ctypes.POINTER(ctypes.POINTER(ctypes.c_uint)) ) ]


class DHDM_DLL_Wrapper:
    dll_path = os.path.join(os.path.dirname(__file__), "dll_dir", "dhdm_dll.dll")

    def __init__(self):
        if not os.path.isfile(self.dll_path):
            raise ValueError("File \"{0}\" not found.".format(self.dll_path))
        try:
            self.dll = ctypes.cdll.LoadLibrary(self.dll_path)
        except OSError as e:
            print("Failed to load dll \"{0}\".".format(self.dll_path))
            raise e

    def generate_disp_morphs(self, gScale, baseMeshPointer, geograft_file,
                                   morphs_groups, morphs_groups_basenames,
                                   vdisp_ts, outputDirpath,
                                   morph_base_until ):

        mesh_info = MeshInfo(gScale, baseMeshPointer, geograft_file=geograft_file)
        morphs_info = MorphsInfo(morphs_groups, morphs_groups_basenames)
        texture_info = TextureInfo(vdisp_ts, outputDirpath)

        r = self.dll.generate_disp_morphs( ctypes.byref(mesh_info),
                                           ctypes.byref(morphs_info),
                                           ctypes.byref(texture_info),
                                           ctypes.c_short(morph_base_until) )

        if r is None or r != 0:
            raise RuntimeError("Function \"{0}\" in \"{1}\" failed.".format("generate_disp_morphs()", self.dll_path))
        return r

    def generate_disp_blender(self, gScale, baseMeshPointer, hdMeshPointer,
                                    vdisp_ts, outputDirpath,
                                    base_name ):

        mesh_info = MeshInfo(gScale, baseMeshPointer, hdMeshPointer=hdMeshPointer)
        texture_info = TextureInfo(vdisp_ts, outputDirpath)

        r = self.dll.generate_disp_blender( ctypes.byref(mesh_info),
                                            ctypes.byref(texture_info),
                                            str_2_char_p(base_name) )

        if r is None or r != 0:
            raise RuntimeError("Function \"{0}\" of DLL \"{1}\" failed.".format("generate_disp_blender()", self.dll_path))
        return r

    def generate_shapes(self, gScale, baseMeshPointer, outputMeshPointer,
                              hd_level, geograft_file,
                              morphs_groups,
                              skn, only_deltas,
                              uv_layer_override=None):

        mesh_info = MeshInfo( gScale, baseMeshPointer, hdMeshPointer=outputMeshPointer,
                              hd_level=hd_level, geograft_file=geograft_file )
        morphs_info = MorphsInfo(morphs_groups, None)

        uv_layer = None
        if uv_layer_override is not None:
            uv_layer = uv_layer_override
        else:
            uv_layer = 0 if skn <= 0 else -1

        r = self.dll.generate_shapes( ctypes.byref(mesh_info),
                                      ctypes.byref(morphs_info),
                                      ctypes.c_short(skn),
                                      ctypes.c_bool(only_deltas),
                                      ctypes.c_short(uv_layer) )

        if r is None or r != 0:
            raise RuntimeError("Function \"{0}\" in \"{1}\" failed.".format("generate_shapes()", self.dll_path))
        return r

    def generate_hd_mesh(self, gScale, baseMeshPointer, outputMeshPointer,
                               hd_level, geograft_file,
                               morphs_groups):

        mesh_info = MeshInfo( gScale, baseMeshPointer, hdMeshPointer=outputMeshPointer,
                              hd_level=hd_level, geograft_file=geograft_file )
        morphs_info = MorphsInfo(morphs_groups, None)

        r = self.dll.generate_hd_mesh( ctypes.byref(mesh_info),
                                       ctypes.byref(morphs_info) )

        if r is None or r != 0:
            raise RuntimeError("Function \"{0}\" in \"{1}\" failed.".format("generate_hd_mesh()", self.dll_path))
        return r

    def generate_rigged_hd_mesh_dae( self, gScale, baseMeshPointer,
                                     hd_level, geograft_file,
                                     morphs_groups,
                                     outputDirpath, outputFilename ):

        mesh_info = MeshInfo( gScale, baseMeshPointer,
                              hd_level=hd_level, geograft_file=geograft_file )
        morphs_info = MorphsInfo( morphs_groups, None )

        r = self.dll.generate_rigged_hd_mesh_dae( ctypes.byref(mesh_info),
                                                  ctypes.byref(morphs_info),
                                                  str_2_char_p(outputDirpath),
                                                  str_2_char_p(outputFilename) )

        if r is None or r != 0:
            raise RuntimeError("Function \"{0}\" in \"{1}\" failed.".format("generate_rigged_hd_mesh_dae()", self.dll_path))
        return r

    def generate_rigged_hd_mesh_begin( self, gScale, baseMeshPointer,
                                       hd_level, geograft_file,
                                       morphs_groups ):

        mesh_info = MeshInfo( gScale, baseMeshPointer,
                              hd_level=hd_level, geograft_file=geograft_file )
        morphs_info = MorphsInfo( morphs_groups, None )

        self.dll.generate_rigged_hd_mesh_begin.restype = ctypes.c_void_p

        r = self.dll.generate_rigged_hd_mesh_begin( ctypes.byref(mesh_info),
                                                    ctypes.byref(morphs_info) )

        if r is None:
            raise RuntimeError("Function \"{0}\" in \"{1}\" failed.".format("generate_rigged_hd_mesh_begin()", self.dll_path))
        bones_weights_info = BoneWeightsInfo.from_address(r)
        return bones_weights_info

    def generate_rigged_hd_mesh_finish( self, gScale, outputMeshPointer,
                                        bones_weights_info ):

        mesh_info = MeshInfo( gScale, None, hdMeshPointer=outputMeshPointer )

        r = self.dll.generate_rigged_hd_mesh_finish( ctypes.byref(mesh_info),
                                                     ctypes.byref(bones_weights_info) )
        if r is None or r != 0:
            raise RuntimeError("Function \"{0}\" in \"{1}\" failed.".format("generate_rigged_hd_mesh_finish()", self.dll_path))
        return r


def call_dll_function(func_name, *args ):
    w = DHDM_DLL_Wrapper()
    func = getattr(w, func_name)
    print("\n---- Start of DLL ----")
    try:
        r = func(*args)
        del w
        print("\n---- End of DLL ----\n")
        return r
    except Exception as e:
        del w
        print("---- ERROR in DLL (read console output).\n")
        raise e

def execute_in_new_thread( func_name, *args ):
    with concurrent.futures.ThreadPoolExecutor(max_workers=1) as executor:
        future = executor.submit( call_dll_function, func_name, *args )
        r = future.result()
    return r
