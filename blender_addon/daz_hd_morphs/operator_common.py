import bpy, os
from . import utils

class VDispTextureSettings:
    def __init__(self, context, tiles):
        addon_props = context.scene.daz_hd_morph
        self.target_space = addon_props.vdisp_target_space
        self.texture_size = int(addon_props.texture_size)
        self.image_type = addon_props.vdisp_image_type
        self.uv_layer = addon_props.texture_uv_layer
        self.max_subd = addon_props.texture_max_subd

        self.scale = 1
        self.midlevel = 0
        if self.image_type == 'P':
            self.auto_settings = addon_props.vdisp_auto_settings
            if not self.auto_settings:
                self.scale = addon_props.vdisp_scale
                self.midlevel = addon_props.vdisp_midlevel
        else:
            self.auto_settings = False

        self.tiles = tiles

    def get_suffix(self):
        suffix = "-{0}_VDISP-{1}_".format(self.target_space, self.texture_size)
        return suffix


class HDMorphOperator(bpy.types.Operator):
    working_dirpath = None
    gScale = None
    geograft_file = None

    vdm_subdirname = "vector_disp_maps"
    normal_subdirname = "normal_maps"
    temporary_subdirname = "_temporary"

    base_ob = None
    base_ob_copy = None

    cleanup_files = None

    morphs_groups = None
    hd_ob = None
    hd_collection_name = "hd_collection"

    vdisp_ts = None
    tiles = None

    saved_settings = None

    @classmethod
    def poll(cls, context):
        return context.mode == 'OBJECT'

    def invoke(self, context, event):
        return self.execute(context)

    def check_input(self, context, check_hd=False):
        scn = context.scene
        addon_props = scn.daz_hd_morph

        if addon_props.base_ob not in scn.objects:
            self.report({'ERROR'}, "Base object not found in the scene.")
            return False
        base_ob = scn.objects[ addon_props.base_ob ]
        if not base_ob or base_ob.type != 'MESH':
            self.report({'ERROR'}, "Invalid base object.")
            return False
        self.base_ob = base_ob

        self.gScale = addon_props.unit_scale

        if check_hd:
            if addon_props.hd_ob not in scn.objects:
                self.report({'ERROR'}, "HD object not found in the scene.")
                return False
            hd_ob = scn.objects[ addon_props.hd_ob ]
            if not hd_ob or hd_ob.type != 'MESH':
                self.report({'ERROR'}, "Invalid HD object.")
                return False
            if len(hd_ob.data.vertices) < len(self.base_ob.data.vertices):
                self.report({'ERROR'}, "HD mesh has fewer vertices than base mesh.")
                return False
            self.hd_ob = hd_ob

        working_dirpath = addon_props.working_dirpath
        if not working_dirpath.strip():
            self.report({'ERROR'}, "Working directory not set.")
            return False
        working_dirpath = os.path.abspath( bpy.path.abspath(working_dirpath) )
        if not os.path.isdir(working_dirpath):
            self.report({'ERROR'}, "Working directory not found.")
            return False
        self.working_dirpath = os.path.join(working_dirpath, utils.makeValidFilename(self.base_ob.name))
        if not os.path.isdir(self.working_dirpath):
            os.mkdir(self.working_dirpath)
        self.cleanup_files = addon_props.delete_temporary_objs

        geograft_file = addon_props.geograft_file
        if geograft_file.strip():
            geograft_file = os.path.abspath( bpy.path.abspath(geograft_file) )
            if not os.path.isfile(geograft_file) or not utils.has_extension(geograft_file, "json"):
                self.report({'ERROR'}, "Invalid geograft file.")
                return False
            self.geograft_file = geograft_file
        else:
            self.geograft_file = None

        self.save_settings(context)

        return True

    def save_settings(self, context):
        s = {}
        if context.scene.render.use_simplify:
            s["simplify"] = True
            context.scene.render.use_simplify = False
        self.saved_settings = s

    def restore_settings(self, context):
        if self.saved_settings is None:
            return
        if "simplify" in self.saved_settings:
            context.scene.render.use_simplify = self.saved_settings["simplify"]
        self.saved_settings = None

    def check_morphs_list(self, context, group_selector=False, can_be_empty=False):
        addon_props = context.scene.daz_hd_morph

        morphs_entries = addon_props.morph_files_list.entries
        morphs_groups = {}
        for e in morphs_entries:
            if group_selector and e.group != addon_props.selected_morphs_group:
                continue
            if e.weight < 1e-3:
                continue
            filepath = e.filepath.strip()
            if not filepath:
                continue
            if not utils.has_extension(filepath, "dsf"):
                self.report({'ERROR'}, "Morph file \"{0}\" has no .dsf extension.".format(os.path.basename(filepath)))
                return False
            filepath = os.path.abspath( bpy.path.abspath( filepath ) )
            if not os.path.isfile(filepath):
                self.report({'ERROR'}, "Morph file \"{0}\" not found.".format(filepath))
                return False
            if e.group not in morphs_groups:
                morphs_groups[e.group] = []
            for fp, _ in morphs_groups[e.group]:
                if fp == filepath:
                    self.report({'ERROR'}, "Morph file \"{0}\" listed more than once in the same group.".format(filepath))
                    return False
            morphs_groups[e.group].append((filepath, e.weight, e.morph_type))

        single_morphs = set()
        for g, morph_tups in morphs_groups.items():
            if len(morph_tups) == 1:
                mfn = os.path.basename(morph_tups[0][0])
                if mfn in single_morphs:
                    self.report({'ERROR'}, "Morph \"{0}\" listed more than once in single morph groups.".format(mfn))
                    return False
                else:
                    single_morphs.add(mfn)

        geograft_vcounts = None
        if self.geograft_file is not None:
            geograft_vcounts = utils.geograft_file_vcounts(self.geograft_file)
        invalid_groups = set()
        for g, morph_tups in morphs_groups.items():
            for dsf_fp, _, _ in morph_tups:
                v_count = utils.dsf_vcount(dsf_fp)
                if v_count is None:
                    print("- Morph \"{}\" IGNORED: describes no geometry.".format(os.path.basename(dsf_fp)))
                    invalid_groups.add(g)
                    continue
                if v_count >= 0:
                    if geograft_vcounts is None:
                        if v_count != len(self.base_ob.data.vertices):
                            print("- Morph \"{}\" IGNORED: base mesh vertex count mismatch.".format(os.path.basename(dsf_fp)))
                            invalid_groups.add(g)
                            continue
                    elif v_count not in geograft_vcounts:
                        print("- Morph \"{}\" IGNORED: geograft file vertex count mismatch.".format(os.path.basename(dsf_fp)))
                        invalid_groups.add(g)
                        continue

        for g in invalid_groups:
            print("-- Morph group '{}' IGNORED: included at least one invalid morph.".format(g))
            del morphs_groups[g]

        if len(morphs_groups) == 0:
            if not can_be_empty:
                self.report({'ERROR'}, "No morph files selected.")
                return False
            else:
                self.morphs_groups = None
                return True

        self.morphs_groups = morphs_groups
        return True

    @staticmethod
    def create_empty_morph_group(outputFilename):
        morphs_groups = {}
        morphs_groups[0] = []
        morphs_groups_filenames = {}
        morphs_groups_filenames[0] = outputFilename
        return morphs_groups, morphs_groups_filenames

    def check_input_vdisp(self, context):
        if not self.check_texture_tiles(context):
            return False
        self.vdisp_ts = VDispTextureSettings(context, self.tiles)
        if not self.check_uv_layer(self.vdisp_ts.uv_layer):
            return False
        return True

    def check_texture_tiles(self, context):
        addon_props = context.scene.daz_hd_morph
        if addon_props.use_texture_tiles:
            self.tiles = []
            for e in addon_props.texture_tiles.entries:
                if e.enabled:
                    self.tiles.append(int(e.name))
            if len(self.tiles) == 0:
                self.report({'ERROR'}, "No UV tiles selected.")
                return False
        else:
            self.tiles = None
        return True

    def check_uv_layer(self, n):
        if n < 0 or not self.base_ob or n >= len(self.base_ob.data.uv_layers):
            self.report({'ERROR'}, "Invalid uv layer.")
            return False
        return True

    def create_subdir(self, subdirpath):
        if not os.path.isdir(subdirpath):
            os.mkdir(subdirpath)
        return subdirpath

    def create_vdm_subdir(self):
        return self.create_subdir( os.path.join(self.working_dirpath, self.vdm_subdirname) )

    def create_normal_subdir(self):
        return self.create_subdir( os.path.join(self.working_dirpath, self.normal_subdirname) )

    def create_temporary_subdir(self):
        return self.create_subdir(self.get_temporary_subdir())

    def get_temporary_subdir(self):
        return os.path.join(self.working_dirpath, self.temporary_subdirname)

    def get_temporary_filepath(self, filename):
        subdirpath = self.create_temporary_subdir()
        return os.path.join(subdirpath, filename)

    def export_temporary_ob(self, ob, write_materials, use_modifiers):
        obj_filename = "{0}.obj".format(utils.makeValidFilename(ob.name))
        obj_filepath = self.get_temporary_filepath(obj_filename)
        obj_filepath_r = utils.export_obj(ob, obj_filepath, write_materials=write_materials, use_modifiers=use_modifiers)
        if obj_filepath_r is None:
            self.report({'ERROR'}, ".obj export failed.")
            return None
        return obj_filepath_r

    def evaluated_base_pointer(self, context, base_modifiers):
        assert( base_modifiers in ('NONE', 'SK', 'NO_SK', 'NO_ARMATURE', 'ALL') )

        if base_modifiers == 'NONE':
            return self.base_ob.as_pointer()
        self.base_ob_copy = utils.copy_object(self.base_ob)
        if base_modifiers == 'SK':
            self.base_ob_copy.modifiers.clear()
        else:
            utils.remove_division_modifiers(self.base_ob_copy)
            if base_modifiers == 'NO_ARMATURE':
                utils.remove_armature_modifiers(self.base_ob_copy)
            elif base_modifiers == 'NO_SK':
                utils.remove_shape_keys(self.base_ob_copy)
        ds = context.evaluated_depsgraph_get()
        return self.base_ob_copy.evaluated_get(ds).as_pointer()

    def create_output_ob( self, hd_level,
                          use_materials=False,
                          empty_vgroups=False,
                          clear_vgroups=False,
                          uv_layers=None ):
        ob_output = utils.copy_object(self.base_ob)

        if not use_materials:
            utils.delete_object_materials(ob_output)

        if clear_vgroups:
            ob_output.vertex_groups.clear()
        elif empty_vgroups:
            utils.empty_all_vgroups(ob_output)

        if uv_layers is not None:
            utils.delete_uv_layers(ob_output, preserve=uv_layers)

        utils.apply_subsurf_modifier(ob_output, hd_level, predelete_all_modifiers=True)
        ob_output.parent = None
        ob_output.data.use_auto_smooth = False
        # ~ utils.remove_non_active_uvs(ob_output)
        return ob_output

    def clean_output_ob(self, ob):
        # ~ ob.data.validate(verbose=True, clean_customdata=True)
        # ~ ob.data.update(calc_edges=True, calc_edges_loose=False)
        # ~ ob.data.validate(verbose=True, clean_customdata=True)
        # ~ utils.delete_loose_edges(ob)
        ob.data.calc_normals()
        utils.remove_daz_properties(ob)

    def cleanup_base_copy(self):
        if self.base_ob_copy is not None:
            utils.delete_object(self.base_ob_copy)
            self.base_ob_copy = None

    def cleanup(self, context):
        self.cleanup_base_copy()
        self.restore_settings(context)

        if self.cleanup_files:
            tmp_dir = self.get_temporary_subdir()
            if not os.path.isdir(tmp_dir):
                return

            fp_to_delete = []
            for fn in os.listdir(tmp_dir):
                fp = os.path.join(tmp_dir, fn)
                if os.path.isfile(fp) and utils.has_extension(fn, "obj", "mtl", "dae"):
                    fp_to_delete.append(fp)

            for fp in fp_to_delete:
                os.remove(fp)
