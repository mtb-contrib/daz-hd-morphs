import os, time
from . import dll_wrapper
from . import utils
from .operator_common import HDMorphOperator


class GenerateHDOperator(HDMorphOperator):
    """Generate HD mesh (with daz's vertex order) from base mesh"""
    bl_idname = "dazhdmorph.generatehd"
    bl_label = "Generate HD mesh"

    hd_level = None
    generate_mode = None
    base_modifiers = None
    use_dae = False
    use_morph_list = False
    import_base_sks = False

    def execute(self, context):
        if not self.check_input(context):
            return {'CANCELLED'}

        addon_props = context.scene.daz_hd_morph
        self.generate_mode = addon_props.generate_mode
        self.hd_level = addon_props.hd_level
        self.base_modifiers = addon_props.base_modifiers
        self.use_morph_list = addon_props.use_morph_list

        if self.use_morph_list:
            if not self.check_morphs_list(context, group_selector=True, can_be_empty=False):
                return {'CANCELLED'}

        t0 = time.perf_counter()
        if self.generate_mode != 'RIGGED':
            r = self.generate_hd_mesh(context)
        else:
            self.use_dae = addon_props.use_dae
            self.import_base_sks = addon_props.import_base_sks
            r = self.generate_rigged_hd_mesh(context)

        self.cleanup(context)
        if not r:
            return {'CANCELLED'}

        self.report({'INFO'}, "HD mesh generated.")
        print("Elapsed: {}".format(time.perf_counter() - t0))
        return {'FINISHED'}

    def generate_rigged_hd_mesh(self, context):
        base_pointer = self.evaluated_base_pointer(context, 'NONE')

        if not self.use_dae:
            hd_ob = self.create_output_ob(self.hd_level, use_materials=True, empty_vgroups=True)
            hd_ob_pointer = hd_ob.as_pointer()
            utils.move_to_collection(hd_ob, self.hd_collection_name)

            bones_weights_info = dll_wrapper.execute_in_new_thread(
                                        "generate_rigged_hd_mesh_begin",
                                        self.gScale, base_pointer,
                                        self.hd_level, self.geograft_file,
                                        self.morphs_groups )

            try:
                utils.create_subd_vgroups(bones_weights_info, hd_ob)
                self.cleanup_base_copy()

            finally:
                dll_wrapper.execute_in_new_thread( "generate_rigged_hd_mesh_finish",
                                                   self.gScale, hd_ob_pointer,
                                                   bones_weights_info )

            self.clean_output_ob(hd_ob)

        else:
            outputDirpath = self.create_temporary_subdir()
            outputFilename = "{0}-div{1}".format(utils.makeValidFilename(self.base_ob.name), self.hd_level)

            dll_wrapper.execute_in_new_thread( "generate_rigged_hd_mesh_dae",
                                               self.gScale, base_pointer,
                                               self.hd_level, self.geograft_file,
                                               self.morphs_groups,
                                               outputDirpath, outputFilename )

            hd_ob = utils.import_dll_collada(os.path.join(outputDirpath, outputFilename + ".dae"))
            utils.move_to_collection(hd_ob, self.hd_collection_name)
            self.cleanup_base_copy()
            utils.copy_uv_layers_names(self.base_ob, hd_ob)
            utils.merge_materials_by_slot(self.base_ob, hd_ob)

        utils.copy_armature(self.base_ob, hd_ob, self.hd_collection_name)
        utils.make_final_active(hd_ob)

        self.reimport_shape_keys(hd_ob, base_pointer)
        return True

    def generate_hd_mesh(self, context):
        if self.generate_mode != 'UNRIGGED':
            self.base_modifiers = 'NONE'

        base_pointer = self.evaluated_base_pointer(context, self.base_modifiers)

        clear_vgroups = (self.generate_mode != 'MULTIRES')
        hd_ob = self.create_output_ob(self.hd_level, use_materials=True, clear_vgroups=clear_vgroups)
        hd_ob.name = "{0}-div{1}".format(utils.makeValidFilename(self.base_ob.name), self.hd_level)
        hd_ob_pointer = hd_ob.as_pointer()
        utils.move_to_collection(hd_ob, self.hd_collection_name)

        dll_wrapper.execute_in_new_thread( "generate_hd_mesh",
                                           self.gScale, base_pointer, hd_ob_pointer,
                                           self.hd_level, self.geograft_file,
                                           self.morphs_groups )

        self.cleanup_base_copy()
        self.clean_output_ob(hd_ob)
        # ~ context.view_layer.depsgraph.update()
        if self.generate_mode == 'MULTIRES':
            utils.create_unsubdivide_multires(hd_ob)
            utils.copy_armature(self.base_ob, hd_ob, self.hd_collection_name, move_to_top=True)

        utils.make_final_active(hd_ob)
        return True

    def reimport_shape_keys(self, hd_ob, base_pointer):
        if not self.import_base_sks:
            return True
        base_sks = utils.get_sk_names(self.base_ob)
        if len(base_sks) == 0:
            return True
        base_sks_map = {}
        for k in base_sks:
            base_sks_map[k.lower()] = k
        del base_sks

        try:
            from import_daz import api
            morphs_dirs = api.get_morph_directories(self.base_ob)
        except:
            print("Required addon \"import_daz\" not found, or api function threw an exception.")
            return False

        sks_pairs = {}
        base_sks_files = {}
        for md in morphs_dirs:
            if not os.path.isdir(md):
                print("Directory \"{0}\" does not exist.".format(md))
                continue

            for root, dirs, files in os.walk(md):
                for f in files:
                    if utils.has_extension(f, "dsf"):
                        fp = os.path.join(root, f)
                        dsf_id = utils.read_dsf_id(fp)
                        if dsf_id is not None:
                            src_sk_name = base_sks_map.get( dsf_id.lower(), None )
                            if src_sk_name is not None:
                                base_sks_files[src_sk_name] = fp
                                sks_pairs[src_sk_name] = src_sk_name
                break
        if len(base_sks_files) == 0:
            print("- Found no base shape keys.")
            return True
        else:
            print("- Found {0} base shape keys. Importing them onto the generated HD mesh...".format(len(base_sks_files)))

        hd_ob_pointer = hd_ob.as_pointer()
        skn = utils.get_shape_key_offset(hd_ob)
        morphs_groups = {}
        for n, sk_id in enumerate(base_sks_files):
            morphs_groups[n] = [ (base_sks_files[sk_id], 1, 'BOTH') ]
            utils.new_shape_key(hd_ob, name=sk_id)

        dll_wrapper.execute_in_new_thread( "generate_shapes",
                                           self.gScale, base_pointer, hd_ob_pointer,
                                           self.hd_level, self.geograft_file,
                                           morphs_groups,
                                           skn, True )

        print("- Setting up drivers...")
        utils.copy_sk_drivers(self.base_ob, hd_ob, sks_pairs)
        return True

class GenerateHDSKOperator(HDMorphOperator):
    bl_description = "Generate morph/s shape key/s on HD mesh (which must have daz's vertex order)"
    bl_idname = "dazhdmorph.hdwithsk"
    bl_label = "Generate shape key/s on HD mesh"

    from_morphs = None
    base_modifiers = None
    hd_level = None

    def execute(self, context):
        if not self.check_input(context, check_hd=True):
            return {'CANCELLED'}

        addon_props = context.scene.daz_hd_morph
        self.from_morphs = (addon_props.sk_source == 'MORPHS')
        self.base_modifiers = addon_props.base_modifiers
        self.copy_base_drivers = addon_props.copy_base_drivers

        if self.from_morphs:
            if not self.check_morphs_list(context):
                return {'CANCELLED'}

        self.hd_level = utils.get_subdivision_level(self.base_ob, self.hd_ob)
        if self.hd_level < 0:
            self.report({'ERROR'}, "Base mesh is more subdivided than hd mesh.")
            return {'CANCELLED'}

        r = self.generate_hd_sk(context)
        self.cleanup(context)
        if not r:
            return {'CANCELLED'}

        self.report({'INFO'}, "HD shape key(s) generated.")
        return {'FINISHED'}

    def generate_hd_sk(self, context):
        outputDirpath = ""
        morphs_groups_filenames = None

        if self.from_morphs:
            # ~ self.base_modifiers = 'NONE'
            morphs_groups_filenames = utils.getMorphsGroupsFilenames( self.morphs_groups,
                                                                      suffix="-div{0}.obj".format(self.hd_level) )
        else:
            outputFilename = "base_shape-div{0}.obj".format(self.hd_level)
            self.morphs_groups, morphs_groups_filenames = self.create_empty_morph_group(outputFilename)

        base_pointer = self.evaluated_base_pointer(context, self.base_modifiers)
        hd_ob_pointer = self.hd_ob.as_pointer()
        skn = utils.get_shape_key_offset(self.hd_ob)

        for g, morph_list in self.morphs_groups.items():
            if self.from_morphs and len(morph_list) == 1:
                sk_name = utils.get_file_id(morph_list[0][0])
            else:
                sk_name = morphs_groups_filenames[g].rsplit(".", 1)[0]
            utils.new_shape_key(self.hd_ob, name=sk_name)

        dll_wrapper.execute_in_new_thread( "generate_shapes",
                                           self.gScale, base_pointer, hd_ob_pointer,
                                           self.hd_level, self.geograft_file,
                                           self.morphs_groups,
                                           skn, self.from_morphs )

        self.cleanup_base_copy()
        if self.from_morphs and self.copy_base_drivers:
            sks_pairs = utils.get_sk_pairs(self.base_ob, self.hd_ob)
            if len(sks_pairs) > 0:
                print("- Setting up drivers...")
                utils.copy_sk_drivers(self.base_ob, self.hd_ob, sks_pairs)
        return True
