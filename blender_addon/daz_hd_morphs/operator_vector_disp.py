import bpy, os, json
from . import dll_wrapper
from . import utils
from .operator_common import HDMorphOperator


class GenerateVectorDispOperator(HDMorphOperator):
    """Generate vector displacement textures from morph/s"""
    bl_idname = "dazhdmorph.vecdisp"
    bl_label = "Generate textures"

    base_modifiers = None
    morph_base_until = None
    json_filename = "Morphs_VDISP.json"

    def execute(self, context):
        if not self.check_input(context):
            return {'CANCELLED'}
        if not self.check_morphs_list(context):
            return {'CANCELLED'}

        addon_props = context.scene.daz_hd_morph
        self.base_modifiers = addon_props.base_modifiers
        self.morph_base_until = addon_props.morph_base_until
        if not self.check_input_vdisp(context):
            return {'CANCELLED'}

        r = self.generate_maps(context)
        self.cleanup(context)
        if not r:
            return {'CANCELLED'}

        self.report({'INFO'}, "Vector displacement maps generated.")
        return {'FINISHED'}

    def generate_vdisp_json(self, outputDirpath, morphs_groups_basenames):
        json_fp = os.path.join(outputDirpath, self.json_filename)
        if os.path.isfile(json_fp):
            with open(json_fp, "r", encoding="utf-8") as f:
                d = json.load(f)
        else:
            d = {"png": {}, "exr": {}}

        if self.vdisp_ts.image_type == 'P':
            d2 = d["png"]
        else:
            d2 = d["exr"]

        for g, morphs in self.morphs_groups.items():
            g_entry = {}
            g_entry["ok"] = False
            g_entry["filepaths"] = [t[0] for t in morphs]
            g_entry["weights"] = [t[1] for t in morphs]
            d2[ morphs_groups_basenames[g] ] = g_entry

        with open(json_fp, "w", encoding="utf-8") as f:
            json.dump(d, f)
        return True

    def generate_maps(self, context):
        base_pointer = self.evaluated_base_pointer(context, self.base_modifiers)

        outputDirpath = self.create_vdm_subdir()

        morphs_groups_basenames = utils.getMorphsGroupsFilenames( self.morphs_groups,
                                                                  suffix=self.vdisp_ts.get_suffix() )

        if not self.generate_vdisp_json(outputDirpath, morphs_groups_basenames):
            return False

        dll_wrapper.execute_in_new_thread( "generate_disp_morphs",
                                           self.gScale, base_pointer, self.geograft_file,
                                           self.morphs_groups, morphs_groups_basenames,
                                           self.vdisp_ts, outputDirpath,
                                           self.morph_base_until )

        return True


class GenerateVectorDispObjObjOperator(HDMorphOperator):
    bl_description = "Generate vector displacement textures from HD mesh's shape.\n"\
                     "If the base mesh and the HD mesh have the same vertex count, they must have the same vertex order.\n"\
                     "Otherwise, the HD mesh must have Blender's subdivision vertex order"
    bl_idname = "dazhdmorph.vecdispobjobj"
    bl_label = "Generate textures"

    base_subd_method = None
    json_filename = "Blender_VDISP.json"

    def execute(self, context):
        if not self.check_input(context, check_hd=True):
            return {'CANCELLED'}

        addon_props = context.scene.daz_hd_morph
        self.base_subd_method = addon_props.base_subdiv_method
        if not self.check_input_vdisp(context):
            return {'CANCELLED'}

        r = self.generate_maps(context)
        self.cleanup(context)
        if not r:
            return {'CANCELLED'}

        self.report({'INFO'}, "Vector displacement maps generated.")
        return {'FINISHED'}

    def generate_vdisp_json(self, outputDirpath, base_name):
        json_fp = os.path.join(outputDirpath, self.json_filename)
        if os.path.isfile(json_fp):
            with open(json_fp, "r", encoding="utf-8") as f:
                d = json.load(f)
        else:
            d = {"png": {}, "exr": {}}

        if self.vdisp_ts.image_type == 'P':
            d2 = d["png"]
        else:
            d2 = d["exr"]

        entry = {}
        entry["ok"] = False
        entry["base_name"] = self.base_ob.name
        entry["hd_name"] = self.hd_ob.name
        d2[ base_name ] = entry

        with open(json_fp, "w", encoding="utf-8") as f:
            json.dump(d, f)
        return True

    def get_level_subdivision_modifier(self, ob):
        subd_level = 0
        subd_type = None
        for m in ob.modifiers:
            if m.type in ('SUBSURF', 'MULTIRES'):
                if subd_type is not None:
                    self.report({'ERROR'}, "Mesh \"{0}\" has more than 1 subdivision modifier.".format(ob.name))
                    return -1, None
                if m.type == 'SUBSURF':
                    subd_level = m.levels
                else:
                    m.levels = m.total_levels
                    subd_level = m.total_levels
                subd_type = m.type
        return subd_level, subd_type

    def generate_maps(self, context):
        subd_diff = utils.get_subdivision_level(self.base_ob, self.hd_ob)
        if subd_diff < 0:
            self.report({'ERROR'}, "Base mesh is more subdivided than hd mesh.")
            return False

        hd_mod_subd_level, hd_subd_type = self.get_level_subdivision_modifier(self.hd_ob)
        if hd_mod_subd_level < 0:
            return False
        if subd_diff > 0 and hd_subd_type is not None:
            self.report({'ERROR'}, "HD mesh is subdivided and also has modifier.")
            return False
        if hd_subd_type is not None:
            subd_diff = hd_mod_subd_level

        base2_ob = utils.copy_object(self.base_ob)
        utils.remove_division_modifiers(base2_ob)
        if subd_diff > 0:
            if self.base_subd_method == 'MULTIRES':
                mr = utils.create_multires_modifier(base2_ob)
                utils.make_single_active(base2_ob)
                for _ in range(0, subd_diff):
                    bpy.ops.object.multires_subdivide(modifier=mr.name, mode='CATMULL_CLARK')
                assert(mr.total_levels == subd_diff)
            else:
                use_limit = (self.base_subd_method == 'SUBSURF_LIMIT')
                sd = utils.create_subsurf_modifier(base2_ob, use_limit=use_limit)
                sd.levels = subd_diff

        base2_ob.location = self.hd_ob.location

        ds = context.evaluated_depsgraph_get()
        hd_ob_eval = self.hd_ob.evaluated_get(ds)
        base2_ob_eval = base2_ob.evaluated_get(ds)

        outputDirpath = self.create_vdm_subdir()
        base_name = "Blender_from_{0}{1}".format( utils.makeValidFilename(self.hd_ob.name),
                                                  self.vdisp_ts.get_suffix() )

        if not self.generate_vdisp_json(outputDirpath, base_name):
            return False

        dll_wrapper.execute_in_new_thread( "generate_disp_blender",
                                           self.gScale, base2_ob_eval.as_pointer(), hd_ob_eval.as_pointer(),
                                           self.vdisp_ts, outputDirpath,
                                           base_name )

        del ds
        del hd_ob_eval
        del base2_ob_eval
        utils.delete_object(base2_ob)
        return True
