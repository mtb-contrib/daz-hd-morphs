#ifndef SHARED_H_INCLUDED
#define SHARED_H_INCLUDED

struct BoneWeightsInfo
{
    void* loadedBaseMeshPointer;
    unsigned int tot_bones;
    unsigned int* tot_bone_verts;
    unsigned int** bone_verts;
};

struct MeshInfo
{
    float gScale;
    void* baseMeshPointer;
    char* geograft_file;

    short hd_level;
    void* hdMeshPointer;
};

struct MorphsInfo
{
    unsigned short groups_n;

    unsigned short* groups_counts;
    char*** morphs_filepaths;
    float** morphs_weights;
    char*** morphs_types;

    char** groups_basenames;
};

struct TextureInfo
{
    char target_space;
    unsigned short texture_size;
    char image_type;
    bool auto_settings;
    float scale;
    float midlevel;
    unsigned short tiles_count;
    unsigned short* tiles;
    short uv_layer;
    short max_subd;

    char* output_dirpath;
};

struct MorphFileInfo
{
    double weight;
    std::string filepath;
    bool use_base;
    bool use_hd;
};

#endif // SHARED_H_INCLUDED
