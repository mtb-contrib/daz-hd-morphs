#include <iostream>
#include <fmt/core.h>

#include "mesh.hh"
#include "utils.hh"

extern "C" {
    #include "DNA_mesh_types.h"
    #include "DNA_meshdata_types.h"
    #include "DNA_object_types.h"
    #include "DNA_key_types.h"
    #include "DNA_listBase.h"
    #include "DNA_customdata_types.h"
    //#include "BLI_listbase.h"
}

/*
    load_uv_layers:
        -1 : none
         0 : all
        n>0: only layer n-1
*/
dhdm::Mesh dhdm::Mesh::fromBlenderPointer( const void * meshObjectPointer,
                                           const short load_uv_layers,
                                           const bool load_materials,
                                           const bool load_vgroups,
                                           const char* geograft_file )
{
    if (meshObjectPointer == nullptr)
        throw std::runtime_error("null pointer");

    const ::Object* const blenderObject = reinterpret_cast<const ::Object*>(meshObjectPointer);
    const ::Mesh* const blenderMesh = (::Mesh*) blenderObject->data;

    dhdm::Mesh mesh;

    const int nverts = blenderMesh -> totvert;

    const MVert* mvert = blenderMesh -> mvert;
    for (int i=0; i < nverts; i++, mvert++)
        mesh.vertices.push_back( {glm::dvec3(mvert->co[0], mvert->co[1], mvert->co[2]) * (1/dhdm::gScale)} );

    const int npoly = blenderMesh->totpoly;
    const MLoop* const mloop = blenderMesh -> mloop;
    const MPoly* mpoly = blenderMesh -> mpoly;

    for (int i=0; i < npoly; i++, mpoly++) {
        const int loopstart = mpoly -> loopstart;
        const int nloop = mpoly -> totloop;
        Face face;
        for (int j=loopstart; j < loopstart + nloop; j++)
            face.vertices.push_back( { .vertex = mloop[j].v, .uv = (UvId) j } );
        face.matId = load_materials ? mpoly->mat_nr : 0;
        mesh.faces.push_back( std::move(face) );
    }
    mesh.uses_materials = load_materials;

    const bool load_uvs = ( load_uv_layers >=0 );
    if (load_uvs)
    {
        const CustomData* const ldata = &(blenderMesh -> ldata);
        const short layer_to_load = load_uv_layers - 1;

        int num_uv_layers = 0;
        for (int i = 0; i < ldata->totlayer; i++)
            if (ldata->layers[i].type == CD_MLOOPUV)
                num_uv_layers++;

        if ( layer_to_load >= 0 && layer_to_load >= num_uv_layers )
            throw std::runtime_error("UV layers error: selected uv layer out of bounds.");

        for (int layer_index = 0; layer_index < num_uv_layers; layer_index++)
        {
            if ( layer_to_load >= 0 && layer_index != layer_to_load)
                continue;

            int i = ldata->typemap[CD_MLOOPUV];
            if ( (i == -1) || (i + layer_index >= ldata->totlayer) ||
                 (ldata->layers[i + layer_index].type != CD_MLOOPUV) )
            {
                throw std::runtime_error("UV layers error.");
            }
            const MLoopUV * const mloopuv = (MLoopUV *) ldata->layers[i + layer_index].data;

            std::vector<UV> uvs;
            mpoly = blenderMesh -> mpoly;
            for (int i=0; i < npoly; i++, mpoly++) {
                const int loopstart = mpoly -> loopstart;
                const int nloop = mpoly -> totloop;
                for (int j=loopstart; j < loopstart + nloop; j++)
                    uvs.push_back( {glm::dvec2(mloopuv[j].uv[0], mloopuv[j].uv[1])} );
            }

            mesh.uv_layers.push_back(std::move(uvs));
        }
    }
    mesh.uses_uvs = load_uvs;

    if (load_vgroups) {
        const ListBase &defbase = blenderObject -> defbase;
        //const int defbase_tot = BLI_listbase_count(&defbase);
        bDeformGroup* dg;
        for (dg = (bDeformGroup*) defbase.first; dg; dg = dg->next)
            mesh.vgroupsNames.push_back(std::string(dg->name));

        const MDeformVert* dvert = blenderMesh -> dvert;
        for (int i=0; i < nverts; i++, dvert++) {
            VertexWeights vw;
            const int totweight = dvert->totweight;
            MDeformWeight* dw = dvert->dw;
            for (int j=0; j < totweight; j++, dw++)
                vw.weights[ dw->def_nr ] = dw->weight;
            mesh.vweights.push_back( std::move(vw) );
        }
        mesh.uses_vgroups = true;
    }

    /*
    std::cout << "Vertices: " << mesh.vertices.size() << std::endl;
    std::cout << "Faces: " << mesh.faces.size() << std::endl;
    std::cout << "UV pairs: " << mesh.uvs.size() << std::endl;
    */

    if (geograft_file != nullptr)
        mesh.load_geo_file(geograft_file);

    return mesh;
}


void dhdm::Mesh::toBlenderMesh( void * meshObjectPointer )
{
    std::cout << "To Blender mesh...\n";
    if (meshObjectPointer == nullptr)
        throw std::runtime_error("null pointer");
    if (!uses_uvs)
        throw std::runtime_error("toBlenderMesh() requires uses_uvs == true");

    ::Object* blenderObject = reinterpret_cast<::Object*>(meshObjectPointer);
    ::Mesh* blenderMesh = (::Mesh*) blenderObject->data;

    const int nverts = blenderMesh->totvert;
    if (vertices.size() != (size_t) nverts)
        throw std::runtime_error("Vertex count mismatch");

    MVert* mvert = blenderMesh -> mvert;
    for (int i=0; i < nverts; i++, mvert++) {
        const glm::dvec3 vertex_pos = vertices[i].pos * dhdm::gScale;
        for (int j=0; j < 3; j++)
            mvert->co[j] = vertex_pos[j];
    }

    const int npoly = blenderMesh->totpoly;
    if (faces.size() != (size_t) npoly)
        throw std::runtime_error("Face count mismatch");

    MPoly* mpoly = blenderMesh -> mpoly;
    MLoop* mloop = blenderMesh -> mloop;
    // MLoopUV *mloopuv = blenderMesh -> mloopuv;

    MEdge* medge = blenderMesh -> medge;
    const int nedges = blenderMesh -> totedge;
    EdgeMap edges_map;

    CustomData* const ldata = &(blenderMesh -> ldata);
    std::vector<MLoopUV *> uv_loops;

    if (uses_uvs)
    {
        int num_uv_layers = 0;
        for (int i = 0; i < ldata->totlayer; i++)
            if (ldata->layers[i].type == CD_MLOOPUV)
                num_uv_layers++;

        for (int layer_index = 0; layer_index < num_uv_layers; layer_index++) {
            int i = ldata->typemap[CD_MLOOPUV];
            if ( (i == -1) || (i + layer_index >= ldata->totlayer) ||
                 (ldata->layers[i + layer_index].type != CD_MLOOPUV) )
            {
                throw std::runtime_error("UV layers error.");
            }
            MLoopUV * mloopuv = (MLoopUV *) ldata->layers[i + layer_index].data;
            uv_loops.push_back(mloopuv);
        }

        if ( uv_layers.size() > uv_loops.size() )
            throw std::runtime_error("uv_layers.size() > uv_loops.size()");
    }

    for (int i=0; i < npoly; i++, mpoly++) {
        const int loopstart = mpoly -> loopstart;
        const int nloop = mpoly -> totloop;
        const Face & face = faces[i];
        mpoly->mat_nr = uses_materials ? face.matId : 0;
        // assert(nloop == 4);

        for (int j=0; j < nloop; j++) {
            const int loopind = loopstart + j;

            /* VERTICES */
            mloop[loopind].v = face.vertices[j].vertex;


            /* EDGES */
            unsigned int medge_index;
            const unsigned int v1 = face.vertices[j].vertex;
            const unsigned int v2 = face.vertices[(j+1) % nloop].vertex;
            Edge poly_edge(std::min(v1, v2), std::max(v1, v2));

            auto it = edges_map.find(poly_edge);
            if ( it == edges_map.end() ) {
                medge_index = edges_map.size();

                if (medge_index >= (unsigned int) nedges) {
                    /*
                    for (auto it = edges_map.begin(); it != edges_map.end(); ++it)
                        std::cout << fmt::format("({}, {}) -> {}\n", std::get<0>(it->first), std::get<1>(it->first), it->second);
                    */
                    throw std::runtime_error(fmt::format("medge_index out of bounds: {} >= {}", medge_index, nedges));
                }

                medge[medge_index].v1 = v1;
                medge[medge_index].v2 = v2;
                edges_map[poly_edge] = medge_index;
            }
            else {
                medge_index = it->second;
            }
            mloop[loopind].e = medge_index;


            /* UV */
            if (uses_uvs)
            {
                const int uvid = face.vertices[j].uv;
                for (size_t layer_n = 0; layer_n < uv_layers.size(); layer_n++)
                {
                    for (int k=0; k < 2; k++)
                        uv_loops[layer_n][loopind].uv[k] = uv_layers[layer_n][uvid].pos[k];
                }
            }

        }
    }

    /* std::cout << fmt::format("Edges map size: {}\n", edges_map.size()); */

    if (uses_vgroups) {
        MDeformVert* dvert = blenderMesh -> dvert;
        for (int i=0; i < nverts; i++, dvert++) {
            const int totweight = dvert->totweight;
            MDeformWeight* dw = dvert->dw;
            const auto & v_weight = vweights[i].weights;
            for (int j=0; j < totweight; j++, dw++) {
                auto it = v_weight.find( dw -> def_nr );
                if (it == v_weight.end())
                    dw -> weight = 0;
                else
                    dw -> weight = it -> second;
            }
        }
    }

}


void dhdm::Mesh::toBlenderShapeKey( void * meshObjectPointer,
                                    const unsigned short offset )
{
    if (meshObjectPointer == nullptr)
        throw std::runtime_error("meshObjectPointer == nullptr");
    /*
    if (offset < 1)
        throw std::runtime_error("Keyblock offset must be > 0");
    */

    ::Object* blenderObject = reinterpret_cast<::Object*>(meshObjectPointer);
    ::Mesh* blenderMesh = (::Mesh*) blenderObject->data;

    const int nverts = blenderMesh->totvert;
    if (vertices.size() != (size_t) nverts)
        throw std::runtime_error("nverts vertex count mismatch");

    KeyBlock* kb = (KeyBlock*) blenderMesh->key->block.first;
    if (offset == 0 && !kb)
        return;

    for(unsigned short i=0; i < offset; i++) {
        if (!(kb = kb->next))
            throw std::runtime_error("Keyblock offset bigger than number of Keyblocks");
    }

    if(kb->totelem != nverts)
        throw std::runtime_error("Keyblock vertex count mismatch");

    float(*fp)[3] = (float(*)[3]) kb->data;

    if (subd_only_deltas)
    {
        for (int i=0; i < nverts; i++, fp++) {
            for (int j=0; j<3; j++)
                (*fp)[j] += vertices[i].pos[j] * dhdm::gScale;
        }
    }
    else
    {
        for (int i=0; i < nverts; i++, fp++) {
            for (int j=0; j<3; j++)
                (*fp)[j] = vertices[i].pos[j] * dhdm::gScale;
        }
    }

}


void dhdm::Mesh::calculate_delta_verts( const void * hdMeshPointer,
                                        std::vector<glm::dvec3> & deltaVertices)
{
    if (hdMeshPointer == nullptr)
        throw std::runtime_error("null pointer");

    const ::Object* blenderObject = reinterpret_cast<const ::Object*>(hdMeshPointer);
    const ::Mesh* hdBlenderMesh = (::Mesh*) blenderObject->data;
    const int nverts = hdBlenderMesh->totvert;
    if (vertices.size() != (size_t) nverts)
        throw std::runtime_error("vertex count mistmatch");

    const MVert* mvert = hdBlenderMesh -> mvert;
    for (int i=0; i < nverts; i++, mvert++) {
        glm::dvec3 delta = glm::dvec3(mvert->co[0], mvert->co[1], mvert->co[2]) - vertices[i].pos * dhdm::gScale;
        deltaVertices.push_back(delta);
    }
}
