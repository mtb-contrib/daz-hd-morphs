#include <optional>
#include <fstream>
#include <iostream>
#include <unordered_map>
#include <glm/gtx/io.hpp>
#include <fmt/format.h>

#include "mesh.hh"
#include "utils.hh"


double dhdm::gScale = 0.01;


dhdm::DhdmFile::DhdmFile(std::string filepath) :
    filepath(filepath), baseVertexCount(0), geo_data(nullptr)
{
}

void dhdm::Mesh::triangulate()
{
    auto nrFaces = faces.size();
    faces.reserve(nrFaces * 2);
    for (size_t i = 0; i < nrFaces; ++i) {
        auto & face = faces[i];
        if (face.vertices.size() == 3) continue;
        assert(face.vertices.size() == 4);
        faces.push_back(Face { .vertices = { face.vertices[2], face.vertices[3], face.vertices[0] } });
        face.vertices.pop_back();
    }
}

void dhdm::Mesh::set_subd_only_deltas(const dhdm::Mesh * originalMesh)
{
    this->originalMesh = originalMesh;
    subd_only_deltas = true;
}

dhdm::Mesh dhdm::Mesh::load_mesh( const char* meshPathC,
                                  const char* uvMapPathC,
                                  const bool load_materials )
{
    const std::string meshPath(meshPathC);
    const std::string ext(meshPath, meshPath.size() - 4);
    if (ext == ".obj") {
        return fromObj( meshPath, load_materials );
    }
    else if (ext == ".dsf") {
        return fromDSF( meshPath, std::string(uvMapPathC) );
    }
    else {
        throw std::runtime_error("Cannot load mesh from file with extension \"" + ext + "\"");
    }
}

dhdm::Mesh dhdm::Mesh::fromObj(const std::string & path, const bool load_materials)
{
    auto fs = std::fstream(path, std::fstream::in);
    if (!fs) throw std::runtime_error("cannot open file");

    dhdm::Mesh mesh;

    std::optional<std::string> objectName;
    std::string line;
    std::unordered_map<std::string, short> materialMap;
    short curr_matId = -1;

    bool first_face = true;
    size_t total_vertices;
    size_t total_uvs;

    std::vector<UV> uv_layer;

    while (std::getline(fs, line))
    {
        if (std::string_view(line).substr(0, 2) == "o ") {
            if (objectName)
                throw std::runtime_error(".obj file contains multiple meshes.");
            objectName = std::string(line, 2);
            std::cout << "Mesh name: " << *objectName << "\n";

        } else if (std::string_view(line).substr(0, 2) == "v ") {
            double x, y, z;
            if (sscanf(line.c_str() + 2, "%lf %lf %lf", &x, &y, &z) != 3)
                throw std::runtime_error("Invalid vertex: " + line);
            mesh.vertices.push_back({ glm::dvec3(x, -z, y) });

        } else if (std::string_view(line).substr(0, 3) == "vt ") {
            double u, v;
            if (sscanf(line.c_str() + 3, "%lf %lf", &u, &v) != 2)
                throw std::runtime_error("Invalid texture coordinate: " + line);
            uv_layer.push_back({ glm::dvec2(u, v) });

        } else if (std::string_view(line).substr(0, 2) == "f ") {
            if (first_face) {
                total_vertices = mesh.vertices.size();
                total_uvs = uv_layer.size();
                first_face = false;
            }

            uint32_t vs[4];
            uint32_t uvs[4];

            int c = sscanf(line.c_str() + 2, "%d/%d %d/%d %d/%d %d/%d", vs, uvs, vs+1, uvs+1, vs+2, uvs+2, vs+3, uvs+3);

            if ( c != 6 && c != 8 )
                throw std::runtime_error("Invalid face: " + line);
            c /= 2;
            Face face;
            for (int i=0; i < c; i++)
            {
                assert(vs[i] >= 1 && vs[i] <= total_vertices);
                assert(uvs[i] >= 1 && uvs[i] <= total_uvs);
                face.vertices.push_back( { .vertex = VertexId(vs[i]-1), .uv = UvId(uvs[i]-1) } );
            }
            face.matId = load_materials ? curr_matId : -1;
            mesh.faces.push_back( std::move(face) );

        } else if (load_materials && std::string_view(line).substr(0, 7) == "usemtl ") {
            std::string matName = std::string(line, 7);

            auto it = materialMap.find(matName);
            if (it != materialMap.end()) {
                curr_matId = it -> second;
            } else {
                mesh.materialNames.push_back(matName);
                curr_matId = (short) (mesh.materialNames.size() - 1);
                materialMap[matName] = curr_matId;
            }
        }

    }
    mesh.uv_layers.push_back(std::move(uv_layer));

    std::cout << fmt::format("Number of vertices: {}\n", mesh.vertices.size());
    std::cout << fmt::format("Number of faces: {}\n", mesh.faces.size());
    if (load_materials) {
        mesh.uses_materials = (mesh.materialNames.size() > 0);
        std::cout << fmt::format("Number of materials: {}\n", mesh.materialNames.size());
    }

    return mesh;
}


void dhdm::Mesh::writeObj(std::ostream & str)
{
    std::cout << "Writing obj...\n";

    for (auto & vert : vertices)
        str << fmt::format("v {} {} {}\n", (float) vert.pos.x, (float) vert.pos.y, (float) vert.pos.z);

    if (uses_uvs && uv_layers.size() > 0) {
        for (auto & uv : uv_layers[0])
            str << fmt::format("vt {} {}\n", (float) uv.pos.x, (float) uv.pos.y);
    }

    if (!uses_materials) {
        for (auto & face : faces) {
            str << "f";
            for (auto & vert : face.vertices)
                str << fmt::format(" {}/{}", vert.vertex + 1, vert.uv + 1);
            str << "\n";
        }
    }
    else {
        std::vector< std::vector<Face> > materials_faces;
        for (auto & face : faces) {
            assert(face.matId >= 0);
            if ( (size_t) face.matId >= materials_faces.size())
                materials_faces.resize(face.matId + 1);
            materials_faces[face.matId].push_back(face);
        }

        const bool has_material_names = (materialNames.size() > 0);

        for (size_t i=0; i < materials_faces.size(); i++) {
            if (has_material_names)
                str << fmt::format("usemtl {}\n", materialNames[i]);
            else
                str << fmt::format("usemtl SLOT_{}\n", i);

            for (auto & face : materials_faces[i]) {
                str << "f";
                for (auto & vert : face.vertices)
                    str << fmt::format(" {}/{}", vert.vertex + 1, vert.uv + 1);
                str << "\n";
            }
        }
    }
}

bool dhdm::Mesh::load_geo_file(const char* geograft_file)
{
    if (geograft_file == nullptr)
        return false;
    const nlohmann::json j = readJSON(std::string(geograft_file));

    for (auto & e : j.items())
    {
        const int32_t nverts = std::stoi(e.key());
        const auto j2 = e.value();
        max_vcount = std::max(max_vcount, nverts);

        std::unordered_map<unsigned int, unsigned int> vmapping;
        for (auto & e2 : j2["vn"])
            vmapping[ e2[0] ] = e2[1];

        std::unordered_map<unsigned int, unsigned int> fmapping;
        for (auto & e2 : j2["fn"])
            fmapping[ e2[0] ] = e2[1];

        vertex_mapping[nverts] = std::move(vmapping);
        face_mapping[nverts] = std::move(fmapping);
    }

    has_geograft = true;
    return true;
}
