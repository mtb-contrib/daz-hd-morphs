---------------------------------------------------
Modified and expanded from: https://github.com/edolstra/dhdm

Compiled with MinGW on Code::Blocks 20.03 in windows x64, with C++17 support.

The following compiled libraries are needed, in order:

    glew32
        http://glew.sourceforge.net

    glfw3
        https://www.glfw.org

    fmt
        https://github.com/fmtlib/fmt

    libpng
        http://www.libpng.org/pub/png/libpng.html

    osdCPU
        https://graphics.pixar.com/opensubdiv/docs/cmake_build.html

    The iostreams part of boost:
        https://www.boost.org/doc/libs/1_75_0/more/getting_started/index.html

    The following libraries included with Code::Blocks MinGW compiler:
        glu32
        opengl32
        gdi32
        user32
        kernel32

And the following headers only libraries:

    https://github.com/g-truc/glm
    https://github.com/nlohmann/json
    https://github.com/syoyo/tinyexr
    Some Blender 2.93 headers (see "./blender_mesh.cc")

---------------------------------------------------
