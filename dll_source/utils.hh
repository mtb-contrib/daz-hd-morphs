#ifndef UTILS_H_INCLUDED
#define UTILS_H_INCLUDED

#include <vector>
#include <string>
#include <tuple>
#include <unordered_map>
#include <set>
#include <nlohmann/json.hpp>

#include "shared.hh"
#include "mesh.hh"


nlohmann::json readJSON(const std::string & path);

void freeVWeightsStruct(BoneWeightsInfo* p);

BoneWeightsInfo* generateVWeightsStruct(dhdm::Mesh* mesh);

void load_morphs_group_info( std::vector<MorphFileInfo> & morphs_group_info,
                             const MorphsInfo* morphs_info,
                             const unsigned int group_number );

void load_tiles_set( std::set<unsigned short> & selected_tiles_set,
                     const TextureInfo* texture_info );

void applySubdivision( dhdm::Mesh & loadedBaseMesh,
                       const MorphsInfo* morphs_info,
                       const short hd_level );

unsigned short applyBaseMorphs( dhdm::Mesh & mesh,
                                std::vector<MorphFileInfo> & morphs_group_info,
                                std::vector<dhdm::DhdmFile> & dhdms );

namespace hash_tuple
{
    template <typename T>
    struct hash_f
    {
        size_t operator()(T const& v) const
        {
            return std::hash<T>()(v);
        }
    };

    namespace
    {
        template <class T>
        inline void hash_combine(size_t& seed, T const& v) {
            seed ^= hash_tuple::hash_f<T>()(v) + 0x9e3779b9 + (seed<<6) + (seed>>2);
        }

        template <class Tuple, size_t Index = std::tuple_size<Tuple>::value - 1>
        struct HashValueImpl
        {
          static void apply(size_t& seed, Tuple const& tup)
          {
            HashValueImpl<Tuple, Index-1>::apply(seed, tup);
            hash_combine(seed, std::get<Index>(tup));
          }
        };

        template <class Tuple>
        struct HashValueImpl<Tuple, 0>
        {
          static void apply(size_t& seed, Tuple const& tup)
          {
            hash_combine(seed, std::get<0>(tup));
          }
        };
    }

    template <typename ... Ts>
    struct hash_f<std::tuple<Ts...>>
    {
        size_t operator()(std::tuple<Ts...> const& tup) const
        {
            size_t seed = 0;
            HashValueImpl<std::tuple<Ts...> >::apply(seed, tup);
            return seed;
        }
    };
}

using Edge = std::tuple<unsigned int, unsigned int>;
using EdgeMap = std::unordered_map< Edge, unsigned int, hash_tuple::hash_f<Edge> >;

#endif // UTILS_H_INCLUDED
