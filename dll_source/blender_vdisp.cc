#include <iostream>
#include <future>

#include "main.hh"
#include "diff.hh"
#include "utils_vdisp.hh"
#include "utils.hh"

DLL_EXPORT int generate_disp_blender( MeshInfo* mesh_info,
                                      const TextureInfo* texture_info,
                                      const char* base_name )
{
    try{
        dhdm::gScale = 1;
        dhdm::Mesh baseMesh = dhdm::Mesh::fromBlenderPointer( mesh_info->baseMeshPointer,
                                                              texture_info->uv_layer,
                                                              false,
                                                              false,
                                                              nullptr );
        dhdm::Mesh hdMesh = dhdm::Mesh::fromBlenderPointer( mesh_info->hdMeshPointer,
                                                            -1,
                                                            false,
                                                            false,
                                                            nullptr );

        if ( baseMesh.vertices.size() != hdMesh.vertices.size() )
        {
            std::cout << "ERROR: Vertex counts of subdivided base mesh and hd mesh don't match.\n";
            return -1;
        }

        const std::string outputDirpath(texture_info->output_dirpath);
        VDISP_Json vd_json(outputDirpath, "Blender");

        std::set<unsigned short> selected_tiles_set;
        load_tiles_set(selected_tiles_set, texture_info);

        std::vector<std::future<void>> asyncs;

        std::cout << "Triangulating...\n";
        baseMesh.triangulate();
        hdMesh.triangulate();

        MeshDiff::renderContextInit(texture_info->image_type, texture_info->texture_size);

        MeshDiff diff( texture_info, baseMesh, hdMesh,
                       base_name );

        diff.updateJson( vd_json.j );
        diff.writeToImage( outputDirpath, selected_tiles_set, asyncs );
        vd_json.save();
        MeshDiff::renderContextFinish();

        std::cout << "Waiting...\n";
        for (auto & async : asyncs)
            async.get();

        return 0;
    } catch (std::exception & e) {
        std::cout << "-Error in DLL: " << e.what() << std::endl;
        return -1;
    }
}
