#include <iostream>
#include <fstream>
#include <iomanip>

#include "utils_vdisp.hh"


SubdivisionVdisp::SubdivisionVdisp(const bool create_subdividers, const dhdm::Mesh * baseMeshPointer) :
    create_subdividers(create_subdividers), baseMeshPointer(baseMeshPointer)
{
}


void SubdivisionVdisp::subdivide_simple(const unsigned int level, dhdm::Mesh & mesh)
{
    if (level < 1) return;

    if (!create_subdividers) {
        mesh.subdivide_simple(level);
        return;
    }

    auto it = subdividers.find(level);
    if (it == subdividers.end())
        subdividers.insert( { level,
                              std::unique_ptr<dhdm::MeshSubdivider>( new dhdm::MeshSubdivider(baseMeshPointer, level) )
                            } );
    subdividers[level] -> subdivide_simple(mesh);
}

void SubdivisionVdisp::subdivide( const unsigned int level, dhdm::Mesh & mesh,
                                  std::vector<dhdm::DhdmFile> & dhdms,
                                  const int apply_edits_until )
{
    if (level < 1) return;

    if (!create_subdividers) {
        mesh.subdivide(level, dhdms, apply_edits_until);
        return;
    }

    auto it = subdividers.find(level);
    if (it == subdividers.end())
        subdividers.insert( { level,
                              std::unique_ptr<dhdm::MeshSubdivider>( new dhdm::MeshSubdivider(baseMeshPointer, level) )
                            } );

    subdividers[level] -> subdivide(mesh, dhdms, apply_edits_until);
}

VDISP_Json::VDISP_Json(const std::string & outputDirpath, const std::string & name):
    json_fp(outputDirpath + "/" + name + "_VDISP.json")
{
    std::ifstream f(json_fp, std::ifstream::in);
    if (!f)
        throw std::runtime_error("Cannot open file \"" + json_fp + "\"");
    f >> j;
    f.close();
}


void VDISP_Json::save()
{
    std::ofstream f(json_fp, std::ofstream::out);
    if (!f)
        throw std::runtime_error("Cannot open file \"" + json_fp + "\"");
    f << std::setw(4) << j << std::endl;
    f.close();
}
