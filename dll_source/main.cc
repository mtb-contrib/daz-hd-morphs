#include <iostream>
#include <fstream>
#include <future>
#include <fmt/format.h>

#include "main.hh"
#include "diff.hh"
#include "utils_vdisp.hh"
#include "utils.hh"

#define DHDM_DEBUG 0


//---------------------- VDISP ----------------------------
DLL_EXPORT int generate_disp_morphs( MeshInfo* mesh_info,
                                     const MorphsInfo* morphs_info,
                                     const TextureInfo* texture_info,
                                     const short morph_base_until )
{
    try{
        dhdm::gScale = mesh_info->gScale;
        //dhdm::Mesh loadedBaseMesh = load_mesh(baseOBJPathC, uvMapPathC, false);
        dhdm::Mesh loadedBaseMesh = dhdm::Mesh::fromBlenderPointer( mesh_info->baseMeshPointer,
                                                                    texture_info->uv_layer,
                                                                    false,
                                                                    false,
                                                                    mesh_info->geograft_file );

        const std::string outputDirpath(texture_info->output_dirpath);
        VDISP_Json vd_json(outputDirpath, "Morphs");

        std::set<unsigned short> selected_tiles_set;
        load_tiles_set(selected_tiles_set, texture_info);

        SubdivisionVdisp sbd(morphs_info->groups_n > 1, &loadedBaseMesh);
        std::vector<std::future<void>> asyncs;

        MeshDiff::renderContextInit(texture_info->image_type, texture_info->texture_size);

        for (unsigned int i=0; i < morphs_info->groups_n; i++) {
            std::cout << "\n--- New morphs group:\n";
            std::vector<MorphFileInfo> morphs_group_info;
            load_morphs_group_info(morphs_group_info, morphs_info, i);

            dhdm::Mesh baseMesh;
            dhdm::Mesh hdMesh = loadedBaseMesh;
            std::vector<dhdm::DhdmFile> dhdms;
            unsigned int level = applyBaseMorphs(hdMesh, morphs_group_info, dhdms);

            if (texture_info->max_subd > 0)
                level = std::min(level, (unsigned int) texture_info->max_subd);

            if (morph_base_until < 0) {
                baseMesh = loadedBaseMesh;
                sbd.subdivide_simple(level, baseMesh);
            }
            else {
                baseMesh = hdMesh;
                if (morph_base_until > 0)
                    sbd.subdivide(level, baseMesh, dhdms, morph_base_until);
                else
                    sbd.subdivide_simple(level, baseMesh);
            }
            sbd.subdivide(level, hdMesh, dhdms, -1);

            std::cout << "Triangulating...\n";
            baseMesh.triangulate();
            hdMesh.triangulate();

            MeshDiff diff( texture_info, baseMesh, hdMesh,
                           morphs_info->groups_basenames[i] );
            diff.updateJson( vd_json.j );
            diff.writeToImage( outputDirpath, selected_tiles_set, asyncs );
        }

        vd_json.save();
        MeshDiff::renderContextFinish();

        std::cout << "Waiting...\n";
        for (auto & async : asyncs)
            async.get();

        return 0;
    } catch (std::exception & e) {
        std::cout << "-Error in DLL: " << e.what() << std::endl;
        return -1;
    }
}


//------------------------- SHAPE ------------------------------
DLL_EXPORT int generate_shapes( MeshInfo* mesh_info,
                                const MorphsInfo* morphs_info,
                                const short skn, const bool only_deltas,
                                const short uv_layer )
{
    try{
        /*
        skn:
             <0 -> morph mesh                       normals single group
              0 -> shape base + morph shape keys    normals multiple groups
             >0 -> morph shape keys                 hd shape keys
        */
        if (mesh_info->hd_level < 0)
            throw std::runtime_error("hd_level must be >= 0.");

        dhdm::gScale = mesh_info->gScale;

        dhdm::Mesh loadedBaseMesh = dhdm::Mesh::fromBlenderPointer( mesh_info->baseMeshPointer,
                                                                    uv_layer,
                                                                    false,
                                                                    false,
                                                                    mesh_info->geograft_file );
        if (only_deltas)
            loadedBaseMesh.set_subd_only_deltas(&loadedBaseMesh);

        std::unique_ptr<dhdm::MeshSubdivider> msd;
        if ( morphs_info->groups_n > 1 )
            msd.reset( new dhdm::MeshSubdivider( &loadedBaseMesh, mesh_info->hd_level ) );

        if (skn == 0)
        {
            dhdm::Mesh subdBaseMesh = loadedBaseMesh;
            if (msd)
                msd->subdivide_simple(subdBaseMesh);
            else
                subdBaseMesh.subdivide_simple(mesh_info->hd_level);

            subdBaseMesh.toBlenderMesh(mesh_info->hdMeshPointer);
            subdBaseMesh.toBlenderShapeKey(mesh_info->hdMeshPointer, 0);
            loadedBaseMesh.uses_uvs = false;
        }

        unsigned short sk_offset = (skn <= 0) ? 1 : (unsigned short) skn;
        for (unsigned int i=0; i < morphs_info->groups_n; i++) {
            std::cout << "\n--- New morphs group:\n";

            std::vector<MorphFileInfo> morphs_group_info;
            load_morphs_group_info(morphs_group_info, morphs_info, i);

            dhdm::Mesh hdMesh = loadedBaseMesh;
            std::vector<dhdm::DhdmFile> dhdms;
            applyBaseMorphs( hdMesh, morphs_group_info, dhdms );

            if (msd)
                msd->subdivide(hdMesh, dhdms, -1);
            else
                hdMesh.subdivide(mesh_info->hd_level, dhdms, -1);

            if (skn >= 0) {
                hdMesh.toBlenderShapeKey( mesh_info->hdMeshPointer, sk_offset );
                sk_offset++;
            }
            else {
                hdMesh.toBlenderMesh( mesh_info->hdMeshPointer );
                // loadedBaseMesh.uses_uvs = false;
                break;
            }

            /*
            std::ofstream outFile;
            outFile.open( filename, std::ofstream::out|std::ofstream::trunc );
            hdMesh.writeObj(outFile);
            outFile.close();
            */
        }

        return 0;
    } catch (std::exception & e) {
        std::cout << "-Error in DLL: " << e.what() << std::endl;
        return -1;
    }
}

DLL_EXPORT int generate_hd_mesh( MeshInfo* mesh_info,
                                 const MorphsInfo* morphs_info )
{
    try{
        dhdm::gScale = mesh_info->gScale;
        dhdm::Mesh loadedBaseMesh = dhdm::Mesh::fromBlenderPointer( mesh_info->baseMeshPointer,
                                                                    0,
                                                                    true,
                                                                    false,
                                                                    mesh_info->geograft_file );

        applySubdivision(loadedBaseMesh, morphs_info, mesh_info->hd_level);

        loadedBaseMesh.toBlenderMesh(mesh_info->hdMeshPointer);
        return 0;
    } catch (std::exception & e) {
        std::cout << "-Error in DLL: " << e.what() << std::endl;
        return 1;
    }
}

DLL_EXPORT int generate_rigged_hd_mesh_dae( MeshInfo* mesh_info,
                                            const MorphsInfo* morphs_info,
                                            const char* output_dirpath,
                                            const char* output_filename )
{
    try{
        dhdm::gScale = mesh_info->gScale;
        dhdm::Mesh loadedBaseMesh = dhdm::Mesh::fromBlenderPointer( mesh_info->baseMeshPointer,
                                                                    0,
                                                                    true,
                                                                    true,
                                                                    mesh_info->geograft_file );

        applySubdivision(loadedBaseMesh, morphs_info, mesh_info->hd_level);

        const std::string filename(output_filename);
        const std::string filepath( std::string(output_dirpath) + "/" + filename + ".dae" );
        loadedBaseMesh.writeCollada( filepath, filename );
        return 0;

    } catch (std::exception & e) {
        std::cout << "-Error in DLL: " << e.what() << std::endl;
        return 1;
    }
}

DLL_EXPORT BoneWeightsInfo* generate_rigged_hd_mesh_begin( MeshInfo* mesh_info,
                                                           const MorphsInfo* morphs_info )
{
    try{
        dhdm::gScale = mesh_info->gScale;
        dhdm::Mesh *loadedBaseMesh = new dhdm::Mesh();
        *loadedBaseMesh = dhdm::Mesh::fromBlenderPointer( mesh_info->baseMeshPointer,
                                                          0,
                                                          true,
                                                          true,
                                                          mesh_info->geograft_file );

        applySubdivision(*loadedBaseMesh, morphs_info, mesh_info->hd_level);

        return generateVWeightsStruct(loadedBaseMesh);

    } catch (std::exception & e) {
        std::cout << "-Error in DLL: " << e.what() << std::endl;
        return nullptr;
    }
}

DLL_EXPORT int generate_rigged_hd_mesh_finish( MeshInfo* mesh_info,
                                               BoneWeightsInfo* bone_weights_info )
{
    try{
        dhdm::gScale = mesh_info->gScale;

        dhdm::Mesh* loadedBaseMesh = (dhdm::Mesh*)  bone_weights_info->loadedBaseMeshPointer;
        loadedBaseMesh->toBlenderMesh(mesh_info->hdMeshPointer);

        delete loadedBaseMesh;
        freeVWeightsStruct(bone_weights_info);
        return 0;

    } catch (std::exception & e) {
        std::cout << "-Error in DLL: " << e.what() << std::endl;
        return 1;
    }
}

//-------------------------------------------------------
